#!/bin/sh

cd "`dirname "$0"`"
export JAVA_HOME=/opt/data/crowd/crowd-java;
exec bin/startup.sh "$@"
