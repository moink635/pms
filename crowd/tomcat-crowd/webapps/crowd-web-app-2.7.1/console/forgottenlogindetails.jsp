<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/webwork" prefix="ww" %>
<html>
<head>
    <title>
        <ww:property value="getText('forgottenlogindetails.title')"/>
    </title>

    <meta name="help.url" content="<ww:text name="help.reset.forgotten.password"/>"/>

    <ww:property value="webResourceManager.requireResource('com.atlassian.auiplugin:jquery')"/>
    <ww:property value="webResourceManager.requireResource('com.atlassian.auiplugin:ajs')"/>
    <ww:property value="webResourceManager.requiredResources" escape="false"/>

    <script>
        var CROWD_FLD = (function () {
            var CrowdFLDConfig = (function () {
                return {
                    PASSWORD_CONTENT_ID: "#forgot-password-info",
                    USERNAME_CONTENT_ID: "#forgot-username-info",
                    PASSWORD_RADIO_ID: "#radioPasswordOption",
                    USERNAME_RADIO_ID: "#radioUsernameOption",
                    SUBMIT_BUTTON_ID: "#submit-btn"
                };
            })();

            function hideAndDisplayDiv(hiddenDiv, displayedDiv) {
                AJS.$(displayedDiv).show();
                AJS.$(hiddenDiv).hide();
            }

            function doForgotPassword() {
                var radioButton = AJS.$(CrowdFLDConfig.PASSWORD_RADIO_ID);
                if (!radioButton.is(':checked')) {
                    radioButton.attr('checked', true);
                }
                hideAndDisplayDiv(CrowdFLDConfig.USERNAME_CONTENT_ID, CrowdFLDConfig.PASSWORD_CONTENT_ID);
                AJS.$(CrowdFLDConfig.SUBMIT_BUTTON_ID).show();
            }

            function doForgotUsername() {
                var radioButton = AJS.$(CrowdFLDConfig.USERNAME_RADIO_ID);
                if (!radioButton.is(':checked')) {
                    radioButton.attr('checked', true);
                }
                hideAndDisplayDiv(CrowdFLDConfig.PASSWORD_CONTENT_ID, CrowdFLDConfig.USERNAME_CONTENT_ID);
                AJS.$(CrowdFLDConfig.SUBMIT_BUTTON_ID).show();
            }

            return {
                doForgotPassword: function () {
                    doForgotPassword();
                },

                doForgotUsername: function () {
                    doForgotUsername();
                },

                initialise: function () {
                    if (AJS.$(CrowdFLDConfig.USERNAME_RADIO_ID).is(':checked')) {
                        doForgotUsername();
                    }
                    else if (AJS.$(CrowdFLDConfig.PASSWORD_RADIO_ID).is(':checked')) {
                        doForgotPassword();
                    }
                    else
                    {
                        AJS.$(CrowdFLDConfig.USERNAME_CONTENT_ID).hide();
                        AJS.$(CrowdFLDConfig.PASSWORD_CONTENT_ID).hide();
                        AJS.$(CrowdFLDConfig.SUBMIT_BUTTON_ID).hide();
                    }

                    AJS.$(CrowdFLDConfig.USERNAME_RADIO_ID).click(function() {
                        doForgotUsername();
                    });

                    AJS.$(CrowdFLDConfig.PASSWORD_RADIO_ID).click(function() {
                        doForgotPassword();
                    });
                }
            };
        })();

        AJS.$(function() {
            CROWD_FLD.initialise();
        });
    </script>
</head>

<body>

<form method="post" action="<ww:url namespace="/console" action="forgottenlogindetails" method="update" />"
      name="forgottenlogindetails">

    <ww:hidden name="%{xsrfTokenName}" value="%{xsrfToken}"/>

    <div class="crowdForm">

        <h2>
            <ww:text name="forgottenlogindetails.title" />
        </h2>

        <div class="formBodyNoTop">

            <ww:component template="form_messages.jsp"/>

            <div class="fieldArea">

                <label for="forgottenDetailSelection" class="fieldLabelArea"><ww:text name="forgottenlogindetails.fieldset.legend.which" /></label>
                <div id="forgottenDetailSelection" class="fieldValueArea">
                    <div class="radio">
                        <input type="radio" id="radioPasswordOption" name="forgottenDetail" value="password" <ww:if test="forgottenDetail == 'password'">checked="checked"</ww:if> /><label for="radioPasswordOption"><ww:text name="forgottenlogindetails.option.password"/></label>
                    </div>
                    <div class="radio">
                        <input type="radio" id="radioUsernameOption" name="forgottenDetail" value="username" <ww:if test="forgottenDetail == 'username'">checked="checked"</ww:if> /><label for="radioUsernameOption"><ww:text name="forgottenlogindetails.option.username"/></label>
                    </div>
                </div>

            </div>

            <div id="forgot-password-info">
                <div class="instruction">
                    <ww:text name="forgottenpassword.desc.line1"/><br/>
                    <ww:text name="forgottenpassword.desc.line2"/>
                </div>
                <ww:textfield name="username" size="30">
                    <ww:param name="label" value="getText('username.label')"/>
                    <ww:param name="required" value="true"/>
                    <ww:param name="description" value="getText('forgottenpassword.username.description')"/>
                </ww:textfield>
            </div>

            <div id="forgot-username-info">
                <div class="instruction">
                    <ww:text name="forgottenusername.desc.line1"/>
                </div>
                <ww:textfield name="email" size="30">
                    <ww:param name="label" value="getText('principal.email.label')"/>
                    <ww:param name="required" value="true"/>
                    <ww:param name="description" value="getText('forgottenusername.email.description')"/>
                </ww:textfield>
            </div>

        </div>
        <div class="formFooter wizardFooter">
            <div class="buttons">
                <input id="submit-btn" type="submit" name="save"
                       value="<ww:property value="getText('continue.label')"/>"/>
            </div>
        </div>

    </div>

</form>

</body>
</html>