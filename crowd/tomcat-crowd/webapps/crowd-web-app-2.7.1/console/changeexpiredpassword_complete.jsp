<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/webwork" prefix="ww" %>
<html>

<head>
    <title><ww:text name="menu.user.console.changepassword.label"/></title>

    <meta name="pagename" content="changepassword"/>
    <meta name="help.url" content="<ww:text name="help.user.console.changepassword"/>"/>
</head>

<body>

<h2><ww:text name="menu.user.console.changepassword.label"/></h2>

<div class="page-content">
    <div class="crowdForm">

        <div class="formBody">

            <ww:component template="form_messages.jsp"/>

            <ww:text name="changepassword.complete.label">
                <ww:param name="0"><a href="<ww:url includeParams="none" namespace="/console" action="login"/>"></ww:param>
                <ww:param name="1"></a></ww:param>
            </ww:text>

        </div>
    </div>
</div>
</body>
</html>
