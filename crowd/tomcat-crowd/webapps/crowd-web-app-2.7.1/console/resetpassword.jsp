<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/webwork" prefix="ww" %>
<html>

<head>
    <title><ww:text name="forgottenpassword.title"/></title>

    <meta name="pagename" content="resetpassword"/>
    <meta name="help.url" content="<ww:text name="help.reset.forgotten.password"/>"/>
</head>

<body>

    <h2><ww:text name="forgottenpassword.title"/></h2>

    <div class="page-content">
        <ww:if test="isInvalidToken">
            <p class="warningBox">
                <ww:text name="forgottenlogindetails.error.expiredtoken"/>
                <br/>
                <a href="<ww:url namespace="/console" includeParams="none" action="forgottenlogindetails"/>"><ww:text name="forgottenpassword.getnewtoken"/></a>
            </p>
        </ww:if>
        <ww:else>
            <div class="crowdForm">
                <form id="general" method="post" action="<ww:url namespace="/console" action="resetpassword" method="update"/>" name="resetpassword">

                    <ww:hidden name="%{xsrfTokenName}" value="%{xsrfToken}"/>

                    <div class="formBody">

                        <ww:component template="form_messages.jsp"/>

                        <ww:if test="%{passwordComplexityMessage != null && !hasFieldErrors()}">
                            <p>
                                <ww:text name="principal.password.complexity.policy"/>
                                <ww:property value="passwordComplexityMessage"/>
                            </p>
                        </ww:if>

                        <ww:password name="password" size="45" required="true">
                            <ww:param name="value" value="password"/>
                            <ww:param name="label">
                                <ww:property value="getText('newpassword.label')"/>
                            </ww:param>
                        </ww:password>

                        <ww:password name="confirmPassword" size="45" required="true">
                            <ww:param name="value" value="confirmPassword"/>
                            <ww:param name="label">
                                <ww:property value="getText('confirmpassword.label')"/>
                            </ww:param>
                        </ww:password>

                        <ww:hidden name="username"/>
                        <ww:hidden name="directoryId"/>
                        <ww:hidden name="token"/>

                    </div>

                    <div class="formFooter wizardFooter">
                        <div class="buttons">
                            <input type="submit" class="button" value="<ww:property value="getText('update.label')"/> &raquo;"/>
                            <input type="button" class="button" id="cancel" value="<ww:property value="getText('cancel.label')"/>" onClick="window.location='<ww:url namespace="/console" action="login" includeParams="none"></ww:url>';"/>
                        </div>
                    </div>

                </form>
            </div>
        </ww:else>
    </div>

</body>
</html>