<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/webwork" prefix="ww" %>
<html>
<head>
    <title>
    <ww:property value="getText('menu.backup.label')"/></title>
    <meta name="section" content="administration" />
    <meta name="pagename" content="backup" />
    <meta name="help.url" content="<ww:property value="getText('help.admin.backup')"/>"/>
</head>

    <body>
            <h2><ww:property value="getText('menu.backup.label')"/></h2>

            <div class="page-content">
                <ww:component template="form_messages.jsp"/>
                <div class="crowdForm">
                    <form method="post" action="<ww:url namespace="/console/secure/admin" action="backup" method="scheduledBackup" includeParams="none" />" name="scheduledBackup">

                        <ww:hidden name="%{xsrfTokenName}" value="%{xsrfToken}"/>
                        <h3><ww:property value="getText('administration.backup.automated.title')"/></h3>

                        <div class="formBody">
                            <ww:checkbox name="enableScheduleBackups">
                                <ww:param name="label" value="getText('administration.backup.automated.enable.description')"/>
                            </ww:checkbox>
                            <ww:select list="scheduledBackupTimes" name="scheduledBackupTime" listKey="key" listValue="value">
                                <ww:param name="label" value="getText('administration.backup.automated.time')"/>
                                <ww:param name="required" value="true"/>
                                <ww:param name="description">
                                    <ww:text name="administration.backup.automated.serverTime">
                                        <ww:param name="value0" value="serverCurrentDate" />
                                    </ww:text>
                                </ww:param>
                            </ww:select>
                        </div>
                        <p>
                            <h4><ww:text name="administration.backup.automated.notes" /></h4>
                            <ul class="bullets">
                                <li><ww:text name="administration.backup.automated.location.info" /></li>
                                <ww:if test="backupSummary.numBackups == 0">
                                    <li><ww:text name="administration.backup.automated.summary.nobackup" /></li>
                                </ww:if>
                                <ww:if test="backupSummary.numBackups > 0">
                                    <li><ww:text name="administration.backup.automated.summary">
                                        <ww:param name="value0" value="backupSummary.latestDate" />
                                        <ww:param name="value1" value="backupSummary.earliestDate" />
                                        <ww:param name="value2" value="backupSummary.numBackups" />
                                    </ww:text></li>
                                </ww:if>
                            </ul>
                        </p>
                        <div class="formFooter wizardFooter">

                            <div class="buttons">
                                <input type="submit" name="scheduledBackup" class="button" value="<ww:property value="getText('update.label')"/> &raquo;"/>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="crowdForm">
                    <h3><ww:property value="getText('administration.backup.manual.title')"/></h3>
                    <form method="post" action="<ww:url namespace="/console/secure/admin" action="backup" method="export" includeParams="none" />" name="export">

                        <ww:hidden name="%{xsrfTokenName}" value="%{xsrfToken}"/>

                        <div class="formBody">
                            <p><ww:text name="administration.backup.text"/></p>

                            <ww:if test="exportResponseMessage != null" >
                                <p class="success"><ww:property value="exportResponseMessage"/></p>
                            </ww:if>

                            <ww:checkbox name="resetDomain" fieldValue="true">
                                <ww:param name="label" value="getText('administration.backup.resetdomain.label')"/>
                                <ww:param name="description">
                                    <ww:property value="getText('administration.backup.resetdomain.description')"/>
                                </ww:param>
                            </ww:checkbox>

                            <ww:textfield name="exportFileName" size="75">
                                <ww:param name="label" value="getText('administration.backup.filename.label')"/>
                                <ww:param name="description">
                                    <ww:property value="getText('administration.backup.filename.pathinfo')"/>
                                </ww:param>
                            </ww:textfield>
                        </div>
                        <div class="formFooter wizardFooter">

                            <div class="buttons">
                                <input type="submit" name="manualBackup" class="button" value="<ww:property value="getText('submit.label')"/> &raquo;"/>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
    </body>
</html>
