<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/webwork" prefix="ww" %>
<html>
<head>
    <title>
        <ww:text name="menu.viewdirectory.label">
            <ww:param><ww:property value="directory.name"/></ww:param>
        </ww:text>
    </title>
    <meta name="section" content="directories"/>
    <meta name="pagename" content="view"/>
    <meta name="help.url" content="<ww:text name="help.directory.internal.permissions"/>"/>
</head>
<body>
<h2>
    <ww:text name="menu.viewdirectory.label">
        <ww:param><ww:property value="directory.name"/></ww:param>
    </ww:text>
</h2>

<div class="page-content">

    <ul class="tabs">

        <li>
            <a id="internal-general" href="<ww:url namespace="/console/secure/directory" action="viewinternal" includeParams="none"><ww:param name="ID" value="ID" /></ww:url>"><ww:text name="menu.details.label"/></a>
        </li>

        <li>
            <a id="internal-configuration" href="<ww:url namespace="/console/secure/directory" action="updateinternalconfiguration" includeParams="none"><ww:param name="ID" value="ID" /></ww:url>"><ww:text name="menu.configuration.label"/></a>
        </li>

        <li class="on">
            <span class="tab"><ww:text name="menu.permissions.label"/></span>
        </li>

        <li>
            <a id="internal-options" href="<ww:url namespace="/console/secure/directory" action="updateinternaloptions" includeParams="none"><ww:param name="ID" value="ID" /></ww:url>"><ww:text name="menu.optional.label"/></a>
        </li>

    </ul>

    <div class="tabContent static">

        <div class="crowdForm">
                <form method="post" name="permissionForm" action="<ww:url namespace="/console/secure/directory" action="updateinternalpermissions" method="update" includeParams="none" />">

                    <ww:hidden name="%{xsrfTokenName}" value="%{xsrfToken}"/>

                    <div class="formBody">

                    <ww:component template="form_messages.jsp"/>

                    <input type="hidden" name="ID" value="<ww:property value="ID" />"/>

                    <ww:component template="permissions.jsp"/>

                </div>
                <div class="formFooter wizardFooter">

                    <div class="buttons">
                        <input name="update-permissions" type="submit" class="button" value="<ww:property value="getText('update.label')"/> &raquo;"/>
                        <input type="button" class="button" value="<ww:property value="getText('cancel.label')"/>"
                               onClick="window.location='<ww:url namespace="/console/secure/directory" action="viewinternal" method="default" includeParams="none" ><ww:param name="ID" value="ID" /></ww:url>';"/>
                    </div>
                </div>

                </form>

                </div>
    </div>

</div>
</body>
</html>