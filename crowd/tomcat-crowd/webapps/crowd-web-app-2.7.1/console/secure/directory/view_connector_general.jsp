<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/webwork" prefix="ww" %>

<html>
<head>
    <title>
        <ww:text name="menu.viewdirectory.label">
            <ww:param><ww:property value="directory.name"/></ww:param>
        </ww:text>
    </title>

    <meta name="section" content="directories"/>
    <meta name="pagename" content="view"/>
    <meta name="help.url" content="<ww:property value="getText('help.directory.connector.details')"/>"/>

    <ww:property value="webResourceManager.requireResource('com.atlassian.auiplugin:jquery')"/>
    <ww:property value="webResourceManager.requireResource('com.atlassian.auiplugin:ajs')"/>
    <ww:property value="webResourceManager.requiredResources" escape="false"/>
</head>
<body>
<h2>
    <ww:text name="menu.viewdirectory.label">
        <ww:param><ww:property value="directory.name"/></ww:param>
    </ww:text>
</h2>

<div class="page-content">

    <ul class="tabs">

        <li class="on">
            <span class="tab"><ww:text name="menu.details.label"/></span>
        </li>

        <li>
            <a id="connector-connectiondetails"
               href="<ww:url namespace="/console/secure/directory" action="updateconnectorconnection" includeParams="none"><ww:param name="ID" value="ID" /></ww:url>"><ww:text
                    name="menu.connector.label"/></a>
        </li>

        <li>
            <a id="connector-configuration"
               href="<ww:url namespace="/console/secure/directory" action="updateconnectorconfiguration" includeParams="none"><ww:param name="ID" value="ID" /></ww:url>"><ww:text
                    name="menu.configuration.label"/></a>
        </li>

        <li>
            <a id="connector-permissions"
               href="<ww:url namespace="/console/secure/directory" action="updateconnectorpermissions" includeParams="none"><ww:param name="ID" value="ID" /></ww:url>"><ww:text
                    name="menu.permissions.label"/></a>
        </li>

        <li>
            <a id="connector-options"
               href="<ww:url namespace="/console/secure/directory" action="updateconnectoroptions" includeParams="none"><ww:param name="ID" value="ID" /></ww:url>"><ww:text
                    name="menu.optional.label"/></a>
        </li>

    </ul>

    <div class="tabContent static" id="tab1">

        <div class="crowdForm">
            <form id="updateGeneral" method="post" action="<ww:url namespace="/console/secure/directory" action="updateconnector" includeParams="none" />">

                <ww:hidden name="%{xsrfTokenName}" value="%{xsrfToken}"/>

                <div class="formBodyNoTop">

                    <ww:component template="form_messages.jsp"/>

                    <input type="hidden" id="directory_id" name="ID" value="<ww:property value="ID" />"/>

                    <ww:textfield name="name" size="50">
                        <ww:param name="required" value="true"/>
                        <ww:param name="label" value="getText('directoryinternal.name.label')"/>
                        <ww:param name="description">
                            <ww:property value="getText('directoryinternal.name.description')"/>
                        </ww:param>
                    </ww:textfield>

                    <ww:textfield name="directoryDescription" size="50">
                        <ww:param name="label" value="getText('directoryinternal.description.label')"/>
                        <ww:param name="description">
                            <ww:property value="getText('directoryinternal.description.description')"/>
                        </ww:param>
                    </ww:textfield>

                    <ww:component template="form_row.jsp">
                        <ww:param name="label" value="getText('directory.type.label')"/>
                        <ww:param name="value" value="directoryImplementationDescriptiveName"/>
                    </ww:component>

                    <ww:checkbox name="active" fieldValue="true">
                        <ww:param name="label" value="getText('directory.active.label')"/>
                        <ww:param name="description">
                            <ww:property value="getText('directory.active.description')"/>
                        </ww:param>
                    </ww:checkbox>

                    <ww:checkbox name="cacheEnabled" fieldValue="true">
                        <ww:param name="label" value="getText('directory.caching.enabled.label')"/>
                        <ww:param name="description">
                            <ww:property value="getText('directory.caching.enabled.description')"/>
                        </ww:param>
                    </ww:checkbox>

                    <ww:if test="cacheEnabled && active">

                        <h3>
                            <ww:if test="currentlySynchronising">
                                <span id="lastSyncLabel" style="display: none"><ww:property value="getText('directory.caching.sync.last.label')"/></span>
                                <span id="activeSyncLabel" style="display: inline"><ww:property value="getText('directory.caching.sync.active.label')"/></span>
                            </ww:if>
                            <ww:else>
                                <span id="lastSyncLabel" style="display: inline"><ww:property value="getText('directory.caching.sync.last.label')"/></span>
                                <span id="activeSyncLabel" style="display: none"><ww:property value="getText('directory.caching.sync.active.label')"/></span>
                            </ww:else>
                        </h3>

                        <ww:component template="form_row.jsp">
                            <ww:param name="label"><ww:property value="getText('directory.caching.sync.start.time.label')"/></ww:param>
                            <ww:param name="value">
                                <span id="startTime"><ww:property value="synchronisationStartTime"/></span>
                                <input type="button"
                                       id="synchroniseDirectoryButton"
                                       style="width: 135px; margin-left: 10px"
                                       value="<ww:property value="getText('directory.caching.sync.now.label')"/>"
                                       onClick="window.location='<ww:url namespace="/console/secure/directory" action="synchconnectorcache" method="default" includeParams="none" ><ww:param name="ID" value="ID" /><ww:param name="%{xsrfTokenName}" value="%{xsrfToken}"/></ww:url>';"/>
                            </ww:param>
                            <ww:param name="escapeValue" value="false" />
                        </ww:component>

                        <ww:component template="form_row.jsp">
                            <ww:param name="label"><ww:property value="getText('directory.caching.sync.duration.label')"/></ww:param>
                            <ww:param name="value">
                                <span id="duration"><ww:property value="synchronisationDuration"/></span>
                            </ww:param>
                            <ww:param name="escapeValue" value="false" />
                        </ww:component>

                        <ww:component template="form_row.jsp">
                            <ww:param name="label"><ww:property value="getText('directory.caching.sync.status.label')"/></ww:param>
                            <ww:param name="value">
                                <span id="status"><ww:property value="synchronisationStatus"/></span>
                            </ww:param>
                            <ww:param name="escapeValue" value="false" />
                        </ww:component>

                    </ww:if>
                </div>

                <div class="formFooter wizardFooter">
                    <div class="buttons">
                        <input type="submit" class="button"
                               value="<ww:property value="getText('update.label')"/> &raquo;"/>
                        <input type="button" class="button" value="<ww:property value="getText('cancel.label')"/>"
                               onClick="window.location='<ww:url namespace="/console/secure/directory" action="viewconnector" method="default" includeParams="none" ><ww:param name="ID" value="ID" /></ww:url>';"/>
                    </div>
                </div>

            </form>
        </div>
    </div>

</div>
<script src="<ww:url value="/console/js/sync_feedback.js"/>"></script>
<script>
    <ww:if test="cacheEnabled && active && currentlySynchronising">
        AJS.$(document).ready(function()
        {
            CROWD_SYNC_FEEDBACK.startSyncFeedback(<ww:property value="ID" />);
        });
    </ww:if>
</script>
</body>
</html>
