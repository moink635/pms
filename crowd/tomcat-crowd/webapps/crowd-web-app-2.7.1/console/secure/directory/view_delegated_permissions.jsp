<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/webwork" prefix="ww" %>

<html>
<head>
    <title>
        <ww:text name="menu.viewdirectory.label">
            <ww:param><ww:property value="directory.name"/></ww:param>
        </ww:text>
    </title>
    <meta name="section" content="directories"/>
    <meta name="pagename" content="view"/>
    <meta name="help.url" content="<ww:property value="getText('help.directory.delegated.permissions')"/>"/>

</head>
<body>
    <h2>
        <ww:text name="menu.viewdirectory.label">
            <ww:param><ww:property value="directory.name"/></ww:param>
        </ww:text>
    </h2>

    <div class="page-content">

        <ul class="tabs">

            <li>
                <a id="delegated-general"
                   href="<ww:url namespace="/console/secure/directory" action="viewdelegated" includeParams="none"><ww:param name="ID" value="ID" /></ww:url>"><ww:text
                        name="menu.details.label"/></a>
            </li>

            <li>
                <a id="delegated-connectiondetails" href="<ww:url namespace="/console/secure/directory" action="updatedelegatedconnection" includeParams="none"><ww:param name="ID" value="ID" /></ww:url>"><ww:text name="menu.connector.label"/></a>
            </li>

            <li>
                <a id="delegated-configuration" href="<ww:url namespace="/console/secure/directory" action="updatedelegatedconfiguration" includeParams="none"><ww:param name="ID" value="ID" /></ww:url>"><ww:text name="menu.configuration.label"/></a>
            </li>

            <li class="on">
                <span class="tab"><ww:text name="menu.permissions.label"/></span>
            </li>

            <li>
                <a id="delegated-options" href="<ww:url namespace="/console/secure/directory" action="updatedelegatedoptions" includeParams="none"><ww:param name="ID" value="ID" /></ww:url>"><ww:text name="menu.optional.label"/></a>
            </li>


        </ul>

        <div class="tabContent static" id="tab1">

            <div class="crowdForm">
                <form id="permissions" method="post" action="<ww:url namespace="/console/secure/directory" action="updatedelegatedpermissions" method="update" includeParams="none" />">

                    <ww:hidden name="%{xsrfTokenName}" value="%{xsrfToken}"/>

                    <div class="formBody">

                        <ww:component template="form_messages.jsp"/>

                        <input type="hidden" name="ID" value="<ww:property value="ID" />"/>

                        <ww:component template="permissions.jsp"/>

                    </div>

                    <div class="formFooter wizardFooter">

                        <div class="buttons">

                            <input type="submit" class="button" value="<ww:text name="update.label"/> &raquo;"/>
                            <input type="button" class="button" value="<ww:text name="cancel.label"/>"
                                   onClick="window.location='<ww:url namespace="/console/secure/directory" action="viewdelegated" method="default" includeParams="none" ><ww:param name="ID" value="ID" /></ww:url>';"/>

                        </div>
                    </div>

                </form>

            </div>

        </div>

    </div>
</body>
</html>