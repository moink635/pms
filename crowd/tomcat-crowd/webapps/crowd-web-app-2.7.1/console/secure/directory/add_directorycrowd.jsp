<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/webwork" prefix="ww" %>

<html>
<head>
    <title>
        <ww:text name="directorycrowdcreate.title"/>
    </title>

    <meta name="section" content="directories"/>
    <meta name="pagename" content="addcrowd"/>
    <meta name="help.url" content="<ww:property value="getText('help.directory.add.crowd.details')"/>"/>

    <script>
        function testConnection()
        {
            document.directorycrowd.action = "<ww:url namespace="/console/secure/directory" action="testcreateremotecrowd" method="testConnection" includeParams="none"/>";

            var tabNumberElement = document.createElement("input");
            tabNumberElement.setAttribute("type", "hidden");
            tabNumberElement.setAttribute("name", "tab");
            tabNumberElement.setAttribute("id", "tab");
            tabNumberElement.setAttribute("value", "2");

            document.directorycrowd.appendChild(tabNumberElement);

            document.directorycrowd.submit();
        }

        function processTabsAndSetHelpLink(tab) {
            switch (tab) {
            case 1:
                setHelpLink('<ww:property value="getText('help.directory.add.crowd.details')"/>'); break;
            case 2:
                setHelpLink('<ww:property value="getText('help.directory.add.crowd.connection')"/>'); break;
            case 3:
                setHelpLink('<ww:property value="getText('help.directory.add.crowd.permissions')"/>'); break;
            }
            processTabs(tab);
        }
    </script>
    <jsp:include page="../../decorator/javascript_tabs.jsp">
        <jsp:param name="totalTabs" value="3"/>
    </jsp:include>
</head>
<body onload="init();">

    <form name="directorycrowd" method="post" action="<ww:url namespace="/console/secure/directory" action="createremotecrowd" method="update" includeParams="none" />">

    <ww:hidden name="%{xsrfTokenName}" value="%{xsrfToken}"/>

    <h2>
        <ww:text name="directorycrowdcreate.title"/>
    </h2>


    <div class="page-content">
        <ul class="tabs">
            <li class="on" id="hreftab1">
                <a href="javascript:processTabsAndSetHelpLink(1);">
                    <ww:property value="getText('menu.details.label')"/>
                </a>
            </li>

            <li id="hreftab2">
                <a href="javascript:processTabsAndSetHelpLink(2);">
                    <ww:property value="getText('menu.connection.label')"/>
                </a>
            </li>

            <li id="hreftab3">
                <a href="javascript:processTabsAndSetHelpLink(3);">
                    <ww:property value="getText('menu.permissions.label')"/>
                </a>
            </li>

        </ul>

        <div class="tabContent" id="tab1">

            <div class="crowdForm">
                <div class="formBody">

                    <ww:component template="form_tab_messages.jsp">
                        <ww:param name="tabID" value="1"/>
                    </ww:component>

                    <ww:textfield name="name" size="50">
                        <ww:param name="label" value="getText('directoryinternal.name.label')"/>
                        <ww:param name="description">
                            <ww:property value="getText('directoryinternal.name.description')"/>
                        </ww:param>
                        <ww:param name="required" value="true" />
                    </ww:textfield>

                    <ww:textfield name="description" size="50">
                        <ww:param name="label" value="getText('directoryinternal.description.label')"/>
                        <ww:param name="description">
                            <ww:property value="getText('directoryinternal.description.description')"/>
                        </ww:param>
                    </ww:textfield>

                    <ww:checkbox name="active" fieldValue="true">
                        <ww:param name="label" value="getText('directory.active.label')"/>
                        <ww:param name="description">
                            <ww:property value="getText('directory.active.description')"/>
                        </ww:param>
                    </ww:checkbox>

                    <ww:checkbox name="cacheEnabled" fieldValue="true">
                        <ww:param name="label" value="getText('directory.caching.enabled.label')"/>
                        <ww:param name="description">
                            <ww:property value="getText('directory.caching.enabled.description')"/>
                        </ww:param>
                    </ww:checkbox>

                    <ww:checkbox name="useNestedGroups" fieldValue="true">
                        <ww:param name="label" value="getText('directoryinternal.nestedgroups.disable.label')"/>
                        <ww:param name="description">
                            <ww:property value="getText('directoryinternal.nestedgroups.disable.description')"/>
                        </ww:param>
                    </ww:checkbox>

                </div>
                <div class="formFooter wizardFooter">

                    <div class="buttons">
                        <input type="submit" class="button" value="<ww:property value="getText('continue.label')"/> &raquo;"/>
                        <input type="button" class="button" value="<ww:property value="getText('cancel.label')"/>" onClick="window.location='<ww:url namespace="/console/secure/directory" action="browse" includeParams="none" />';"/>
                    </div>
                </div>

            </div>

        </div>

        <div class="tabContent" id="tab2">

            <div class="crowdForm">
                <div class="formBody">

                    <ww:component template="form_tab_messages.jsp">
                        <ww:param name="tabID" value="2"/>
                    </ww:component>

                    <ww:textfield name="url" size="50">
                        <ww:param name="label" value="getText('directorycrowd.url.label')"/>
                        <ww:param name="description">
                            <ww:property value="getText('directorycrowd.url.description')"/>
                        </ww:param>
                        <ww:param name="required" value="true" />
                    </ww:textfield>

                    <ww:textfield name="applicationName" size="50">
                        <ww:param name="label" value="getText('directorycrowd.applicationname.label')"/>
                        <ww:param name="description">
                            <ww:property value="getText('directorycrowd.applicationname.description')"/>
                        </ww:param>
                        <ww:param name="required" value="true" />
                    </ww:textfield>

                    <ww:password name="applicationPassword" size="50">
                        <ww:param name="label" value="getText('directorycrowd.applicationpassword.label')"/>
                        <ww:param name="description">
                            <ww:property value="getText('directorycrowd.applicationpassword.description')"/>
                        </ww:param>
                        <ww:param name="required" value="true" />
                    </ww:password>

                    <ww:hidden name="savedApplicationPassword" value="%{applicationPassword}" />

                    <ww:textfield name="httpTimeout">
                        <ww:param name="label" value="getText('directorycrowd.http.timeout.label')"/>
                        <ww:param name="description">
                            <ww:property value="getText('directorycrowd.http.timeout.description')"/>
                        </ww:param>
                    </ww:textfield>

                    <ww:textfield name="httpMaxConnections">
                        <ww:param name="label" value="getText('directorycrowd.http.maxconnections.label')"/>
                        <ww:param name="description">
                            <ww:property value="getText('directorycrowd.http.maxconnections.description')"/>
                        </ww:param>
                    </ww:textfield>

                    <ww:textfield name="httpProxyHost" size="50">
                        <ww:param name="label" value="getText('directorycrowd.proxy.host.label')"/>
                        <ww:param name="description">
                            <ww:property value="getText('directorycrowd.proxy.host.description')"/>
                        </ww:param>
                    </ww:textfield>

                    <ww:textfield name="httpProxyPort">
                        <ww:param name="label" value="getText('directorycrowd.proxy.port.label')"/>
                        <ww:param name="description">
                            <ww:property value="getText('directorycrowd.proxy.port.description')"/>
                        </ww:param>
                    </ww:textfield>

                    <ww:textfield name="httpProxyUsername">
                        <ww:param name="label" value="getText('directorycrowd.proxy.username.label')"/>
                        <ww:param name="description">
                            <ww:property value="getText('directorycrowd.proxy.username.description')"/>
                        </ww:param>
                    </ww:textfield>

                    <ww:password name="httpProxyPassword">
                        <ww:param name="label" value="getText('directorycrowd.proxy.password.label')"/>
                        <ww:param name="description">
                            <ww:property value="getText('directorycrowd.proxy.password.description')"/>
                        </ww:param>
                    </ww:password>

                    <ww:checkbox name="incrementalSyncEnabled" fieldValue="true">
                        <ww:param name="label" value="getText('directorycrowd.incrementalsync.enable.label')"/>
                        <ww:param name="description">
                            <ww:property value="getText('directorycrowd.incrementalsync.enable.description')"/>
                        </ww:param>
                    </ww:checkbox>

                    <div id="polling_interval">
                    <ww:textfield name="pollingIntervalInMin">
                        <ww:param name="label" value="getText('directorycrowd.polling.interval.label')"/>
                        <ww:param name="description">
                            <ww:property value="getText('directorycrowd.polling.interval.description')"/>
                        </ww:param>
                    </ww:textfield>
                    </div>

                    <div class="textFieldButton buttons" style="">
                        <input id="test-connection" type="button" class="button" style="width: 125px;" value="<ww:property value="getText('directorycrowd.testconnection.label')"/>" onClick="testConnection();"/>
                    </div>

                </div>
                <div class="formFooter wizardFooter">

                    <div class="buttons">
                        <input type="submit" class="button" value="<ww:property value="getText('continue.label')"/> &raquo;"/>
                        <input type="button" class="button" value="<ww:property value="getText('cancel.label')"/>" onClick="window.location='<ww:url namespace="/console/secure/directory" action="browse" includeParams="none" />';"/>
                    </div>
                </div>

            </div>

        </div>

        <div class="tabContent" id="tab3">

            <div class="crowdForm">
                <div class="formBody">

                    <ww:component template="form_tab_messages.jsp">
                        <ww:param name="tabID" value="3"/>
                    </ww:component>

                    <ww:component template="permissions.jsp"/>

                </div>
                <div class="formFooter wizardFooter">

                    <div class="buttons">

                    <input type="submit" class="button" value="<ww:property value="getText('continue.label')"/> &raquo;"/>
                    <input type="button" class="button" value="<ww:property value="getText('cancel.label')"/>" onClick="window.location='<ww:url namespace="/console/secure/directory" action="browse" includeParams="none" />';"/>
                </div>
            </div>

            </div>

        </div>
    </div>

    </form>

</body>
</html>
