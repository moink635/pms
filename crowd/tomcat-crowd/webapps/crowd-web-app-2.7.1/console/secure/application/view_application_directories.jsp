<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/webwork" prefix="ww" %>
<html>
<head>
    <title>
        <ww:text name="menu.viewapplication.label"/>
    </title>
    <meta name="section" content="applications"/>
    <meta name="pagename" content="view"/>
    <meta name="help.url" content="<ww:text name="help.application.view.directories"/>"/>

    <script>
        function addDirectory()
        {
            var form = document.directoriesForm;
            form.action = '<ww:url namespace="/console/secure/application" action="updatedirectory" method="addDirectory" includeParams="none" />';
            form.submit();
        }
    </script>
</head>
<body>


<h2 id="application-name">
    <img class="application-icon" style="padding-bottom:3px;" title="<ww:property value="getImageTitle(application.active, application.type)"/>"
         alt="<ww:property value="getImageTitle(application.active, application.type)"/>" src="<ww:property value="getImageLocation(application.active, application.type)" />"/>
    <ww:property value="application.name"/>
</h2>

<div class="page-content">

    <ww:component template="application_tab_headers.jsp">
        <ww:param name="pagekey" value="'application-directories'"/>
    </ww:component>

    <div class="tabContent static" id="tab2">

        <div class="crowdForm">

            <form method="post"
                  action="<ww:url namespace="/console/secure/application" action="updatedirectory" method="update" includeParams="none"/>"
                  name="directoriesForm">

                <ww:hidden name="%{xsrfTokenName}" value="%{xsrfToken}"/>

                <div class="formBodyNoTop">

                    <ww:component template="form_messages.jsp"/>

                    <ww:if test="!isIncrementalSyncAvailable">
                        <p class="noteBox">
                            <ww:property value="getText('application.directorymappings.no.incremental.sync')"/>
                        </p>
                    </ww:if>

                    <p>
                        <ww:property value="getText('application.directorymappings.text')"/>
                    </p>

                    <input type="hidden" name="ID" value="<ww:property value="ID" />"/>

                    <table id="directoriesTable" class="formTable">

                        <ww:if test="application.directoryMappings.size > 1">
                            <tr>
                                <th width="40%">
                                    <ww:property value="getText('browser.directory.label')"/>
                                </th>
                                <th width="15%">
                                    <ww:property value="getText('browser.directoryorder.label')"/>
                                </th>
                                <th width="25%">
                                    <ww:property value="getText('browser.allowalltoauthenticate.label')"/>
                                </th>
                                <th width="20%">
                                    <ww:property value="getText('browser.action.label')"/>
                                </th>
                            </tr>
                        </ww:if>
                        <ww:else>
                            <tr>
                                <th width="55%">
                                    <ww:property value="getText('browser.directory.label')"/>
                                </th>
                                <th width="25%">
                                    <ww:property value="getText('browser.allowalltoauthenticate.label')"/>
                                </th>
                                <th width="20%">
                                    <ww:property value="getText('browser.action.label')"/>
                                </th>
                            </tr>
                        </ww:else>

                        <ww:iterator value="application.directoryMappings" status="rowstatus">

                            <input type="hidden" name="directory<ww:property value="#rowstatus.count" />" value="<ww:property value="directory.id" />"/>

                            <ww:if test="#rowstatus.odd == true">
                                <tr class="odd">
                            </ww:if>
                            <ww:else>
                                <tr class="even">
                            </ww:else>

                            <td>
                                <ww:property value="directory.name"/>
                            </td>

                            <ww:if test="application.directoryMappings.size > 1">
                                <td style="text-align: center;">
                                    <ww:if test="#rowstatus.count > 1 && #rowstatus.count != 0">

                                        <a id="up-<ww:property value="directory.id"/>"
                                           href="<ww:url namespace="/console/secure/application" action="updatedirectory" method="up" includeParams="none">
                                                     <ww:param name="ID" value="ID"/>
                                                     <ww:param name="directoryID" value="directory.id"/>
                                                     <ww:param name="%{xsrfTokenName}" value="%{xsrfToken}"/>
                                                 </ww:url>"
                                           name="<ww:property value="getText('moveup.label')" />"><img
                                                src="<ww:url value="/console/images/icons/16x16/arrow_up_blue.gif" />"
                                                alt="<ww:property value="getText('moveup.label')" />"/></a>
                                    </ww:if>

                                    <ww:if test="#rowstatus.last == false">

                                        <ww:if test="#rowstatus.first == false">
                                            &nbsp;
                                        </ww:if>
                                        <a id="down-<ww:property value="directory.id"/>"
                                           href="<ww:url namespace="/console/secure/application" action="updatedirectory" method="down" includeParams="none">
                                                     <ww:param name="ID" value="ID"/>
                                                     <ww:param name="directoryID" value="directory.id"/>
                                                     <ww:param name="tab" value="2"/>
                                                     <ww:param name="%{xsrfTokenName}" value="%{xsrfToken}"/>
                                                 </ww:url>"
                                           name="<ww:property value="getText('movedown.label')" />"><img
                                                src="<ww:url value="/console/images/icons/16x16/arrow_down_blue.gif" />"
                                                alt="<ww:property value="getText('movedown.label')" />"/></a>
                                    </ww:if>
                                </td>
                            </ww:if>

                            <td>
                                <select name="directory<ww:property value="#rowstatus.count" />-allowAll" style="width: 130px;">
                                    <option value="true"
                                            <ww:if test="allowAllToAuthenticate == true">selected</ww:if>
                                            >
                                        <ww:property value="getText('true.label')"/>
                                    </option>
                                    <option value="false"
                                            <ww:if test="allowAllToAuthenticate == false">selected</ww:if>
                                            >
                                        <ww:property value="getText('false.label')"/>
                                    </option>
                                </select>
                            </td>


                            <td>
                                <a href="<ww:url namespace="/console/secure/application" action="updatedirectory" method="removeDirectory" includeParams="none">
                                             <ww:param name="ID" value="ID"/>
                                             <ww:param name="directoryID" value="directory.id"/>
                                             <ww:param name="%{xsrfTokenName}" value="%{xsrfToken}"/>
                                         </ww:url>"
                                   title="<ww:property value="getText('remove.label')"/>">
                                    <ww:property value="getText('remove.label')"/>
                                </a>

                            </td>

                            </tr>

                        </ww:iterator>

                    </table>
                </div>

                <div class="formFooter wizardFooter">

                    <div class="buttons">
                        <ww:if test="unsubscribedDirectories.size > 0">
                            <select name="unsubscribedDirectoriesID" style="width: 270px;">
                                <ww:iterator value="unsubscribedDirectories">
                                    <option value="<ww:property value="id" />">
                                        <ww:property value="name"/>
                                    </option>
                                </ww:iterator>
                            </select>

                            <input id="add-directory" type="button" class="button" value="<ww:property value="getText('add.label')"/> &raquo;"
                                   onClick="addDirectory();"/>
                            &nbsp;&nbsp;&nbsp;
                        </ww:if>
                        <ww:if test="application.directoryMappings.size > 0">
                            <input type="submit" class="button" style="float:right;" value="<ww:property value="getText('update.label')"/> &raquo;"/>
                        </ww:if>
                    </div>
                </div>

            </form>

        </div>
    </div>


</div>
</body>
</html>