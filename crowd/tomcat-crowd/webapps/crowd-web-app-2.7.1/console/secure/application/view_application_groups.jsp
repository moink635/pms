<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/webwork" prefix="ww" %>
<html>
<head>
    <title>
        <ww:text name="menu.viewapplication.label"/>
    </title>
    <meta name="section" content="applications"/>
    <meta name="pagename" content="view"/>
    <meta name="help.url" content="<ww:text name="help.application.view.groups"/>"/>

    <script>

        function addGroup()
        {
            var form = document.groupsForm;
            form.action = '<ww:url namespace="/console/secure/application" action="updategroups" method="addGroup" includeParams="none"/>';
            form.submit();
        }

    </script>
</head>
<body>


<h2 id="application-name">
    <img class="application-icon" style="padding-bottom:3px;" title="<ww:property value="getImageTitle(application.active, application.type)"/>" alt="<ww:property value="getImageTitle(application.active, application.type)"/>"
         src="<ww:property value="getImageLocation(application.active, application.type)" />"/>
    <ww:property value="application.name"/>
</h2>

<div class="page-content">

    <ww:component template="application_tab_headers.jsp">
        <ww:param name="pagekey" value="'application-groups'"/>
    </ww:component>

    <div class="tabContent static" id="tab3">

        <div class="crowdForm">

            <form name="groupsForm" method="post" action="<ww:url namespace="/console/secure/application" action="updategroups" method="update" includeParams="none"/>">

                <ww:hidden name="%{xsrfTokenName}" value="%{xsrfToken}"/>

                <div class="formBodyNoTop">

                    <ww:component template="form_messages.jsp"/>

                    <p>
                        <ww:property value="getText('application.groupmappings.text')"/>
                    </p>

                    <input type="hidden" name="ID" value="<ww:property value="ID" />"/>
                    <input type="hidden" name="tab" value="3"/>

                    <table id="groupsTable" class="formTable">
                        <tr>
                            <th width="82%">
                                <ww:property value="getText('browser.directory.label')"/>&nbsp;-&nbsp;<ww:property value="getText('browser.group.label')"/>
                            </th>
                            <th width="18%">
                                <ww:property value="getText('browser.action.label')"/>
                            </th>
                        </tr>

                        <ww:iterator value="groupMappingsForApplication" status="rowstatus">

                            <input type="hidden" name="group<ww:property value="#rowstatus.count" />"
                                   value="<ww:property value="directory.id" />"/>

                            <ww:if test="#rowstatus.odd == true">
                                <tr class="odd">
                            </ww:if>
                            <ww:else>
                                <tr class="even">
                            </ww:else>

                            <td>
                                <a href="<ww:url namespace="/console/secure/group" action="view"><ww:param name="directoryID" value="directory.id"/><ww:param name="name" value="groupName"/></ww:url>">
                                <ww:property value="directory.name"/>&nbsp;-&nbsp;<ww:property value="groupName"/>
                                </a>
                            </td>

                            <td>
                                <a id="remove-<ww:property value="groupName"/>" href="<ww:url namespace="/console/secure/application" action="updategroups" method="removeGroup" includeParams="none" >
                                                                                          <ww:param name="ID" value="application.id"/>
                                                                                          <ww:param name="removeDirectoryID" value="directory.id"/>
                                                                                          <ww:param name="removeName" value="groupName"/>
                                                                                          <ww:param name="tab" value="3"/>
                                                                                          <ww:param name="%{xsrfTokenName}" value="%{xsrfToken}"/>
                                                                                      </ww:url>"
                                   title="<ww:property value="getText('remove.label')"/> <ww:property value="groupName"/>">
                                    <ww:property value="getText('remove.label')"/>
                                </a>
                            </td>

                            </tr>

                        </ww:iterator>

                    </table>
                </div>

                <div class="formFooter wizardFooter">
                    <div class="buttons">
                        <ww:if test="unsubscribedGroups.size > 0">
                            <select name="unsubscribedGroup" style="width: 270px;">
                                <ww:iterator value="unsubscribedGroups">
                                    <option value="<ww:property value="directory.id" />-<ww:property value="groupName" />">
                                        <ww:property value="directory.name"/>
                                        &nbsp;&ndash;&nbsp;
                                        <ww:property value="groupName"/>
                                    </option>
                                </ww:iterator>
                            </select>

                            <input id="add-group" type="button" class="button" value="<ww:property value="getText('add.label')"/> &raquo;" onClick="addGroup();"/>
                        </ww:if>
                    </div>
                </div>

            </form>
        </div>
    </div>

</div>
</body>
</html>