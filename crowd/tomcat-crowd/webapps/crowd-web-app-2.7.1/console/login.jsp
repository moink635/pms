<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/webwork" prefix="ww" %>
<html>
    <head>
        <title><ww:property value="getText('login.title')"/></title>

        <meta name="help.url" content="<ww:property value="getText('help.login')"/>"/>

        <ww:property value="webResourceManager.requireResource('com.atlassian.auiplugin:jquery')"/>
        <ww:property value="webResourceManager.requireResource('com.atlassian.auiplugin:ajs')"/>
        <ww:property value="webResourceManager.requiredResources" escape="false"/>

        <script>
            AJS.$(function() {
                var $usernameField = AJS.$('#j_username');
                if ($usernameField.val() === "") {
                    $usernameField.focus();
                }
                else {
                    AJS.$('#j_password').focus();
                }
            });
        </script>
    </head>

    <body>

    <!-- The double use of console is necessary to get Webwork 2.2.6 to render the form action url properly. -->
    <form method="post" action="<ww:url namespace="/console" value="/console/j_security_check"/>" id="login" name="login">

        <ww:hidden name="%{xsrfTokenName}" value="%{xsrfToken}"/>

        <div class="crowdForm">

            <h2><ww:text name="login.title"/> <ww:text name="login.to"/> <ww:property value="applicationName"/></h2>

            <div class="formBodyNoTop">

                <ww:if test="!domainValid">
                    <p class="warningBox">
                        <ww:text name="invaliddommain.label">
                            <ww:param name="0"><ww:property value="applicationName"/></ww:param>
                            <ww:param name="1"><a href="<ww:text name="help.prefix"/><ww:text name="help.admin.ssodomain"/>" target="_crowdhelp"></ww:param>
                            <ww:param name="2"></a></ww:param>
                        </ww:text>
                    </p>
                </ww:if>

                <ww:component template="form_messages.jsp"/>
                <ww:if test="#request['passwordUpdateSuccessful'] || #parameters['passwordUpdateSuccessful']">
                    <p class="informationBox">
                        <ww:property value="getText('resetpassword.complete.label')"/>
                    </p>
                </ww:if>

                <ww:textfield name="j_username" size="30">
                    <ww:param name="label" value="getText('username.label')"/>
                    <ww:param name="required" value="true"/>
                </ww:textfield>

                <ww:password name="j_password" size="30">
                    <ww:param name="label" value="getText('password.label')"/>
                    <ww:param name="required" value="true"/>
                </ww:password>

            </div>
            
            <ww:if test="showForgotPassword">
            <div class="fieldArea">
                <div class="secondColumn"><a id="forgottenlogindetails" href="<ww:url namespace="/console" action="forgottenlogindetails" method="default"/>"><ww:text name="forgottenlogindetails.link.label" /></a></div>
            </div>
            </ww:if>

            <div class="formFooter wizardFooter">
                <div class="buttons">
                    <input type="submit" class="button" value="<ww:property value="getText('login.label')"/> &raquo;"/>
                </div>
            </div>

        </div>

    </form>

    </body>
</html>
