JAVA_OPTS="-Xms128m -Xmx512m -XX:MaxPermSize=256m -Dfile.encoding=UTF-8 $JAVA_OPTS"

export JAVA_OPTS
export JAVA_HOME=/opt/data/crowd/crowd-java;
export PATH=$JAVA_HOME:bin/$PATH;

