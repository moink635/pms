package org.apache.jsp.template.xhtml;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class checkbox_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(2);
    _jspx_dependants.add("/template/xhtml/controlheader.jsp");
    _jspx_dependants.add("/template/xhtml/controlfooter.jsp");
  }

  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest;
  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fwebwork_005fiterator_0026_005fvalue;
  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fwebwork_005fproperty_005fnobody;
  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody;
  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fescape_005fnobody;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fwebwork_005fiterator_0026_005fvalue = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fwebwork_005fproperty_005fnobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fescape_005fnobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.release();
    _005fjspx_005ftagPool_005fwebwork_005fiterator_0026_005fvalue.release();
    _005fjspx_005ftagPool_005fwebwork_005fproperty_005fnobody.release();
    _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.release();
    _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fescape_005fnobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write('\n');
      out.write("\n");
      out.write("\n");
      out.write("<div class=\"fieldArea required\">\n");
      out.write("\n");
      out.write("    ");
      if (_jspx_meth_webwork_005fif_005f0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("\n");
      out.write("    <label class=\"fieldLabelArea\" for=\"");
      if (_jspx_meth_webwork_005fproperty_005f1(_jspx_page_context))
        return;
      out.write("\">\n");
      out.write("\n");
      out.write("        ");
      if (_jspx_meth_webwork_005fif_005f1(_jspx_page_context))
        return;
      out.write("\n");
      out.write("\n");
      out.write("        ");
      if (_jspx_meth_webwork_005fproperty_005f2(_jspx_page_context))
        return;
      out.write(":</label>\n");
      out.write("\n");
      out.write("            <div class=\"fieldValueArea\">\n");
      out.write("\n");
      out.write("\n");
      out.write("    <input type=\"checkbox\"\n");
      out.write("        ");
      if (_jspx_meth_webwork_005fif_005f2(_jspx_page_context))
        return;
      out.write("\n");
      out.write("        name=\"");
      if (_jspx_meth_webwork_005fproperty_005f3(_jspx_page_context))
        return;
      out.write("\"\n");
      out.write("        value=\"");
      if (_jspx_meth_webwork_005fproperty_005f4(_jspx_page_context))
        return;
      out.write("\"\n");
      out.write("        ");
      if (_jspx_meth_webwork_005fif_005f3(_jspx_page_context))
        return;
      out.write("\n");
      out.write("        ");
      if (_jspx_meth_webwork_005fif_005f4(_jspx_page_context))
        return;
      out.write("\n");
      out.write("        ");
      if (_jspx_meth_webwork_005fif_005f5(_jspx_page_context))
        return;
      out.write("\n");
      out.write("        ");
      if (_jspx_meth_webwork_005fif_005f6(_jspx_page_context))
        return;
      out.write("\n");
      out.write("        ");
      if (_jspx_meth_webwork_005fif_005f7(_jspx_page_context))
        return;
      out.write("\n");
      out.write("/>\n");
      out.write("\n");
      out.write("\n");
      out.write("        <div class=\"fieldDescription\">\n");
      out.write("            ");
      if (_jspx_meth_webwork_005fproperty_005f9(_jspx_page_context))
        return;
      out.write("</div>\n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("</div>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_webwork_005fif_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  webwork:if
    com.opensymphony.webwork.views.jsp.IfTag _jspx_th_webwork_005fif_005f0 = (com.opensymphony.webwork.views.jsp.IfTag) _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.get(com.opensymphony.webwork.views.jsp.IfTag.class);
    _jspx_th_webwork_005fif_005f0.setPageContext(_jspx_page_context);
    _jspx_th_webwork_005fif_005f0.setParent(null);
    // /template/xhtml/controlheader.jsp(5,4) name = test type = null reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_webwork_005fif_005f0.setTest("fieldErrors[parameters['name']] != null");
    int _jspx_eval_webwork_005fif_005f0 = _jspx_th_webwork_005fif_005f0.doStartTag();
    if (_jspx_eval_webwork_005fif_005f0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_webwork_005fif_005f0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_webwork_005fif_005f0.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_webwork_005fif_005f0.doInitBody();
      }
      do {
        out.write("\n");
        out.write("    <div class=\"errorBox\">\n");
        out.write("        ");
        if (_jspx_meth_webwork_005fiterator_005f0(_jspx_th_webwork_005fif_005f0, _jspx_page_context))
          return true;
        out.write("\n");
        out.write("    </div>\n");
        out.write("    ");
        int evalDoAfterBody = _jspx_th_webwork_005fif_005f0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_webwork_005fif_005f0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.popBody();
      }
    }
    if (_jspx_th_webwork_005fif_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.reuse(_jspx_th_webwork_005fif_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.reuse(_jspx_th_webwork_005fif_005f0);
    return false;
  }

  private boolean _jspx_meth_webwork_005fiterator_005f0(javax.servlet.jsp.tagext.JspTag _jspx_th_webwork_005fif_005f0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  webwork:iterator
    com.opensymphony.webwork.views.jsp.IteratorTag _jspx_th_webwork_005fiterator_005f0 = (com.opensymphony.webwork.views.jsp.IteratorTag) _005fjspx_005ftagPool_005fwebwork_005fiterator_0026_005fvalue.get(com.opensymphony.webwork.views.jsp.IteratorTag.class);
    _jspx_th_webwork_005fiterator_005f0.setPageContext(_jspx_page_context);
    _jspx_th_webwork_005fiterator_005f0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_webwork_005fif_005f0);
    // /template/xhtml/controlheader.jsp(7,8) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_webwork_005fiterator_005f0.setValue("fieldErrors[parameters['name']]");
    int _jspx_eval_webwork_005fiterator_005f0 = _jspx_th_webwork_005fiterator_005f0.doStartTag();
    if (_jspx_eval_webwork_005fiterator_005f0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_webwork_005fiterator_005f0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_webwork_005fiterator_005f0.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_webwork_005fiterator_005f0.doInitBody();
      }
      do {
        out.write("\n");
        out.write("            ");
        if (_jspx_meth_webwork_005fproperty_005f0(_jspx_th_webwork_005fiterator_005f0, _jspx_page_context))
          return true;
        out.write("<br />\n");
        out.write("        ");
        int evalDoAfterBody = _jspx_th_webwork_005fiterator_005f0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_webwork_005fiterator_005f0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.popBody();
      }
    }
    if (_jspx_th_webwork_005fiterator_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fwebwork_005fiterator_0026_005fvalue.reuse(_jspx_th_webwork_005fiterator_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fwebwork_005fiterator_0026_005fvalue.reuse(_jspx_th_webwork_005fiterator_005f0);
    return false;
  }

  private boolean _jspx_meth_webwork_005fproperty_005f0(javax.servlet.jsp.tagext.JspTag _jspx_th_webwork_005fiterator_005f0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  webwork:property
    com.opensymphony.webwork.views.jsp.PropertyTag _jspx_th_webwork_005fproperty_005f0 = (com.opensymphony.webwork.views.jsp.PropertyTag) _005fjspx_005ftagPool_005fwebwork_005fproperty_005fnobody.get(com.opensymphony.webwork.views.jsp.PropertyTag.class);
    _jspx_th_webwork_005fproperty_005f0.setPageContext(_jspx_page_context);
    _jspx_th_webwork_005fproperty_005f0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_webwork_005fiterator_005f0);
    int _jspx_eval_webwork_005fproperty_005f0 = _jspx_th_webwork_005fproperty_005f0.doStartTag();
    if (_jspx_th_webwork_005fproperty_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fwebwork_005fproperty_005fnobody.reuse(_jspx_th_webwork_005fproperty_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fwebwork_005fproperty_005fnobody.reuse(_jspx_th_webwork_005fproperty_005f0);
    return false;
  }

  private boolean _jspx_meth_webwork_005fproperty_005f1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  webwork:property
    com.opensymphony.webwork.views.jsp.PropertyTag _jspx_th_webwork_005fproperty_005f1 = (com.opensymphony.webwork.views.jsp.PropertyTag) _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.get(com.opensymphony.webwork.views.jsp.PropertyTag.class);
    _jspx_th_webwork_005fproperty_005f1.setPageContext(_jspx_page_context);
    _jspx_th_webwork_005fproperty_005f1.setParent(null);
    // /template/xhtml/controlheader.jsp(13,39) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_webwork_005fproperty_005f1.setValue("parameters['name']");
    int _jspx_eval_webwork_005fproperty_005f1 = _jspx_th_webwork_005fproperty_005f1.doStartTag();
    if (_jspx_th_webwork_005fproperty_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.reuse(_jspx_th_webwork_005fproperty_005f1);
      return true;
    }
    _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.reuse(_jspx_th_webwork_005fproperty_005f1);
    return false;
  }

  private boolean _jspx_meth_webwork_005fif_005f1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  webwork:if
    com.opensymphony.webwork.views.jsp.IfTag _jspx_th_webwork_005fif_005f1 = (com.opensymphony.webwork.views.jsp.IfTag) _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.get(com.opensymphony.webwork.views.jsp.IfTag.class);
    _jspx_th_webwork_005fif_005f1.setPageContext(_jspx_page_context);
    _jspx_th_webwork_005fif_005f1.setParent(null);
    // /template/xhtml/controlheader.jsp(15,8) name = test type = null reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_webwork_005fif_005f1.setTest("parameters['required'] == true");
    int _jspx_eval_webwork_005fif_005f1 = _jspx_th_webwork_005fif_005f1.doStartTag();
    if (_jspx_eval_webwork_005fif_005f1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_webwork_005fif_005f1 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_webwork_005fif_005f1.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_webwork_005fif_005f1.doInitBody();
      }
      do {
        out.write("\n");
        out.write("            <span class=\"required\">*</span>\n");
        out.write("        ");
        int evalDoAfterBody = _jspx_th_webwork_005fif_005f1.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_webwork_005fif_005f1 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.popBody();
      }
    }
    if (_jspx_th_webwork_005fif_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.reuse(_jspx_th_webwork_005fif_005f1);
      return true;
    }
    _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.reuse(_jspx_th_webwork_005fif_005f1);
    return false;
  }

  private boolean _jspx_meth_webwork_005fproperty_005f2(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  webwork:property
    com.opensymphony.webwork.views.jsp.PropertyTag _jspx_th_webwork_005fproperty_005f2 = (com.opensymphony.webwork.views.jsp.PropertyTag) _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.get(com.opensymphony.webwork.views.jsp.PropertyTag.class);
    _jspx_th_webwork_005fproperty_005f2.setPageContext(_jspx_page_context);
    _jspx_th_webwork_005fproperty_005f2.setParent(null);
    // /template/xhtml/controlheader.jsp(19,8) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_webwork_005fproperty_005f2.setValue("parameters['label']");
    int _jspx_eval_webwork_005fproperty_005f2 = _jspx_th_webwork_005fproperty_005f2.doStartTag();
    if (_jspx_th_webwork_005fproperty_005f2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.reuse(_jspx_th_webwork_005fproperty_005f2);
      return true;
    }
    _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.reuse(_jspx_th_webwork_005fproperty_005f2);
    return false;
  }

  private boolean _jspx_meth_webwork_005fif_005f2(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  webwork:if
    com.opensymphony.webwork.views.jsp.IfTag _jspx_th_webwork_005fif_005f2 = (com.opensymphony.webwork.views.jsp.IfTag) _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.get(com.opensymphony.webwork.views.jsp.IfTag.class);
    _jspx_th_webwork_005fif_005f2.setPageContext(_jspx_page_context);
    _jspx_th_webwork_005fif_005f2.setParent(null);
    // /template/xhtml/checkbox.jsp(5,8) name = test type = null reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_webwork_005fif_005f2.setTest("parameters['nameValue'] == true");
    int _jspx_eval_webwork_005fif_005f2 = _jspx_th_webwork_005fif_005f2.doStartTag();
    if (_jspx_eval_webwork_005fif_005f2 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_webwork_005fif_005f2 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_webwork_005fif_005f2.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_webwork_005fif_005f2.doInitBody();
      }
      do {
        out.write("checked=\"checked\"");
        int evalDoAfterBody = _jspx_th_webwork_005fif_005f2.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_webwork_005fif_005f2 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.popBody();
      }
    }
    if (_jspx_th_webwork_005fif_005f2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.reuse(_jspx_th_webwork_005fif_005f2);
      return true;
    }
    _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.reuse(_jspx_th_webwork_005fif_005f2);
    return false;
  }

  private boolean _jspx_meth_webwork_005fproperty_005f3(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  webwork:property
    com.opensymphony.webwork.views.jsp.PropertyTag _jspx_th_webwork_005fproperty_005f3 = (com.opensymphony.webwork.views.jsp.PropertyTag) _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.get(com.opensymphony.webwork.views.jsp.PropertyTag.class);
    _jspx_th_webwork_005fproperty_005f3.setPageContext(_jspx_page_context);
    _jspx_th_webwork_005fproperty_005f3.setParent(null);
    // /template/xhtml/checkbox.jsp(6,14) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_webwork_005fproperty_005f3.setValue("parameters['name']");
    int _jspx_eval_webwork_005fproperty_005f3 = _jspx_th_webwork_005fproperty_005f3.doStartTag();
    if (_jspx_th_webwork_005fproperty_005f3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.reuse(_jspx_th_webwork_005fproperty_005f3);
      return true;
    }
    _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.reuse(_jspx_th_webwork_005fproperty_005f3);
    return false;
  }

  private boolean _jspx_meth_webwork_005fproperty_005f4(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  webwork:property
    com.opensymphony.webwork.views.jsp.PropertyTag _jspx_th_webwork_005fproperty_005f4 = (com.opensymphony.webwork.views.jsp.PropertyTag) _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.get(com.opensymphony.webwork.views.jsp.PropertyTag.class);
    _jspx_th_webwork_005fproperty_005f4.setPageContext(_jspx_page_context);
    _jspx_th_webwork_005fproperty_005f4.setParent(null);
    // /template/xhtml/checkbox.jsp(7,15) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_webwork_005fproperty_005f4.setValue("parameters['fieldValue']");
    int _jspx_eval_webwork_005fproperty_005f4 = _jspx_th_webwork_005fproperty_005f4.doStartTag();
    if (_jspx_th_webwork_005fproperty_005f4.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.reuse(_jspx_th_webwork_005fproperty_005f4);
      return true;
    }
    _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.reuse(_jspx_th_webwork_005fproperty_005f4);
    return false;
  }

  private boolean _jspx_meth_webwork_005fif_005f3(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  webwork:if
    com.opensymphony.webwork.views.jsp.IfTag _jspx_th_webwork_005fif_005f3 = (com.opensymphony.webwork.views.jsp.IfTag) _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.get(com.opensymphony.webwork.views.jsp.IfTag.class);
    _jspx_th_webwork_005fif_005f3.setPageContext(_jspx_page_context);
    _jspx_th_webwork_005fif_005f3.setParent(null);
    // /template/xhtml/checkbox.jsp(8,8) name = test type = null reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_webwork_005fif_005f3.setTest("parameters['disabled'] == true");
    int _jspx_eval_webwork_005fif_005f3 = _jspx_th_webwork_005fif_005f3.doStartTag();
    if (_jspx_eval_webwork_005fif_005f3 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_webwork_005fif_005f3 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_webwork_005fif_005f3.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_webwork_005fif_005f3.doInitBody();
      }
      do {
        out.write("disabled=\"disabled\"");
        int evalDoAfterBody = _jspx_th_webwork_005fif_005f3.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_webwork_005fif_005f3 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.popBody();
      }
    }
    if (_jspx_th_webwork_005fif_005f3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.reuse(_jspx_th_webwork_005fif_005f3);
      return true;
    }
    _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.reuse(_jspx_th_webwork_005fif_005f3);
    return false;
  }

  private boolean _jspx_meth_webwork_005fif_005f4(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  webwork:if
    com.opensymphony.webwork.views.jsp.IfTag _jspx_th_webwork_005fif_005f4 = (com.opensymphony.webwork.views.jsp.IfTag) _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.get(com.opensymphony.webwork.views.jsp.IfTag.class);
    _jspx_th_webwork_005fif_005f4.setPageContext(_jspx_page_context);
    _jspx_th_webwork_005fif_005f4.setParent(null);
    // /template/xhtml/checkbox.jsp(9,8) name = test type = null reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_webwork_005fif_005f4.setTest("parameters['tabindex'] != null");
    int _jspx_eval_webwork_005fif_005f4 = _jspx_th_webwork_005fif_005f4.doStartTag();
    if (_jspx_eval_webwork_005fif_005f4 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_webwork_005fif_005f4 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_webwork_005fif_005f4.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_webwork_005fif_005f4.doInitBody();
      }
      do {
        out.write("tabindex=\"");
        if (_jspx_meth_webwork_005fproperty_005f5(_jspx_th_webwork_005fif_005f4, _jspx_page_context))
          return true;
        out.write('"');
        int evalDoAfterBody = _jspx_th_webwork_005fif_005f4.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_webwork_005fif_005f4 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.popBody();
      }
    }
    if (_jspx_th_webwork_005fif_005f4.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.reuse(_jspx_th_webwork_005fif_005f4);
      return true;
    }
    _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.reuse(_jspx_th_webwork_005fif_005f4);
    return false;
  }

  private boolean _jspx_meth_webwork_005fproperty_005f5(javax.servlet.jsp.tagext.JspTag _jspx_th_webwork_005fif_005f4, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  webwork:property
    com.opensymphony.webwork.views.jsp.PropertyTag _jspx_th_webwork_005fproperty_005f5 = (com.opensymphony.webwork.views.jsp.PropertyTag) _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.get(com.opensymphony.webwork.views.jsp.PropertyTag.class);
    _jspx_th_webwork_005fproperty_005f5.setPageContext(_jspx_page_context);
    _jspx_th_webwork_005fproperty_005f5.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_webwork_005fif_005f4);
    // /template/xhtml/checkbox.jsp(9,68) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_webwork_005fproperty_005f5.setValue("parameters['tabindex']");
    int _jspx_eval_webwork_005fproperty_005f5 = _jspx_th_webwork_005fproperty_005f5.doStartTag();
    if (_jspx_th_webwork_005fproperty_005f5.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.reuse(_jspx_th_webwork_005fproperty_005f5);
      return true;
    }
    _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.reuse(_jspx_th_webwork_005fproperty_005f5);
    return false;
  }

  private boolean _jspx_meth_webwork_005fif_005f5(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  webwork:if
    com.opensymphony.webwork.views.jsp.IfTag _jspx_th_webwork_005fif_005f5 = (com.opensymphony.webwork.views.jsp.IfTag) _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.get(com.opensymphony.webwork.views.jsp.IfTag.class);
    _jspx_th_webwork_005fif_005f5.setPageContext(_jspx_page_context);
    _jspx_th_webwork_005fif_005f5.setParent(null);
    // /template/xhtml/checkbox.jsp(10,8) name = test type = null reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_webwork_005fif_005f5.setTest("parameters['onchange'] != null");
    int _jspx_eval_webwork_005fif_005f5 = _jspx_th_webwork_005fif_005f5.doStartTag();
    if (_jspx_eval_webwork_005fif_005f5 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_webwork_005fif_005f5 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_webwork_005fif_005f5.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_webwork_005fif_005f5.doInitBody();
      }
      do {
        out.write("onchange=\"");
        if (_jspx_meth_webwork_005fproperty_005f6(_jspx_th_webwork_005fif_005f5, _jspx_page_context))
          return true;
        out.write('"');
        int evalDoAfterBody = _jspx_th_webwork_005fif_005f5.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_webwork_005fif_005f5 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.popBody();
      }
    }
    if (_jspx_th_webwork_005fif_005f5.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.reuse(_jspx_th_webwork_005fif_005f5);
      return true;
    }
    _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.reuse(_jspx_th_webwork_005fif_005f5);
    return false;
  }

  private boolean _jspx_meth_webwork_005fproperty_005f6(javax.servlet.jsp.tagext.JspTag _jspx_th_webwork_005fif_005f5, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  webwork:property
    com.opensymphony.webwork.views.jsp.PropertyTag _jspx_th_webwork_005fproperty_005f6 = (com.opensymphony.webwork.views.jsp.PropertyTag) _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.get(com.opensymphony.webwork.views.jsp.PropertyTag.class);
    _jspx_th_webwork_005fproperty_005f6.setPageContext(_jspx_page_context);
    _jspx_th_webwork_005fproperty_005f6.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_webwork_005fif_005f5);
    // /template/xhtml/checkbox.jsp(10,68) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_webwork_005fproperty_005f6.setValue("parameters['onchange']");
    int _jspx_eval_webwork_005fproperty_005f6 = _jspx_th_webwork_005fproperty_005f6.doStartTag();
    if (_jspx_th_webwork_005fproperty_005f6.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.reuse(_jspx_th_webwork_005fproperty_005f6);
      return true;
    }
    _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.reuse(_jspx_th_webwork_005fproperty_005f6);
    return false;
  }

  private boolean _jspx_meth_webwork_005fif_005f6(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  webwork:if
    com.opensymphony.webwork.views.jsp.IfTag _jspx_th_webwork_005fif_005f6 = (com.opensymphony.webwork.views.jsp.IfTag) _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.get(com.opensymphony.webwork.views.jsp.IfTag.class);
    _jspx_th_webwork_005fif_005f6.setPageContext(_jspx_page_context);
    _jspx_th_webwork_005fif_005f6.setParent(null);
    // /template/xhtml/checkbox.jsp(11,8) name = test type = null reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_webwork_005fif_005f6.setTest("parameters['onclick'] != null");
    int _jspx_eval_webwork_005fif_005f6 = _jspx_th_webwork_005fif_005f6.doStartTag();
    if (_jspx_eval_webwork_005fif_005f6 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_webwork_005fif_005f6 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_webwork_005fif_005f6.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_webwork_005fif_005f6.doInitBody();
      }
      do {
        out.write("onclick=\"");
        if (_jspx_meth_webwork_005fproperty_005f7(_jspx_th_webwork_005fif_005f6, _jspx_page_context))
          return true;
        out.write('"');
        int evalDoAfterBody = _jspx_th_webwork_005fif_005f6.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_webwork_005fif_005f6 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.popBody();
      }
    }
    if (_jspx_th_webwork_005fif_005f6.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.reuse(_jspx_th_webwork_005fif_005f6);
      return true;
    }
    _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.reuse(_jspx_th_webwork_005fif_005f6);
    return false;
  }

  private boolean _jspx_meth_webwork_005fproperty_005f7(javax.servlet.jsp.tagext.JspTag _jspx_th_webwork_005fif_005f6, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  webwork:property
    com.opensymphony.webwork.views.jsp.PropertyTag _jspx_th_webwork_005fproperty_005f7 = (com.opensymphony.webwork.views.jsp.PropertyTag) _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.get(com.opensymphony.webwork.views.jsp.PropertyTag.class);
    _jspx_th_webwork_005fproperty_005f7.setPageContext(_jspx_page_context);
    _jspx_th_webwork_005fproperty_005f7.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_webwork_005fif_005f6);
    // /template/xhtml/checkbox.jsp(11,66) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_webwork_005fproperty_005f7.setValue("parameters['onclick']");
    int _jspx_eval_webwork_005fproperty_005f7 = _jspx_th_webwork_005fproperty_005f7.doStartTag();
    if (_jspx_th_webwork_005fproperty_005f7.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.reuse(_jspx_th_webwork_005fproperty_005f7);
      return true;
    }
    _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.reuse(_jspx_th_webwork_005fproperty_005f7);
    return false;
  }

  private boolean _jspx_meth_webwork_005fif_005f7(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  webwork:if
    com.opensymphony.webwork.views.jsp.IfTag _jspx_th_webwork_005fif_005f7 = (com.opensymphony.webwork.views.jsp.IfTag) _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.get(com.opensymphony.webwork.views.jsp.IfTag.class);
    _jspx_th_webwork_005fif_005f7.setPageContext(_jspx_page_context);
    _jspx_th_webwork_005fif_005f7.setParent(null);
    // /template/xhtml/checkbox.jsp(12,8) name = test type = null reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_webwork_005fif_005f7.setTest("parameters['id'] != null");
    int _jspx_eval_webwork_005fif_005f7 = _jspx_th_webwork_005fif_005f7.doStartTag();
    if (_jspx_eval_webwork_005fif_005f7 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_webwork_005fif_005f7 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_webwork_005fif_005f7.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_webwork_005fif_005f7.doInitBody();
      }
      do {
        out.write("id=\"");
        if (_jspx_meth_webwork_005fproperty_005f8(_jspx_th_webwork_005fif_005f7, _jspx_page_context))
          return true;
        out.write('"');
        int evalDoAfterBody = _jspx_th_webwork_005fif_005f7.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_webwork_005fif_005f7 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.popBody();
      }
    }
    if (_jspx_th_webwork_005fif_005f7.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.reuse(_jspx_th_webwork_005fif_005f7);
      return true;
    }
    _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.reuse(_jspx_th_webwork_005fif_005f7);
    return false;
  }

  private boolean _jspx_meth_webwork_005fproperty_005f8(javax.servlet.jsp.tagext.JspTag _jspx_th_webwork_005fif_005f7, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  webwork:property
    com.opensymphony.webwork.views.jsp.PropertyTag _jspx_th_webwork_005fproperty_005f8 = (com.opensymphony.webwork.views.jsp.PropertyTag) _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.get(com.opensymphony.webwork.views.jsp.PropertyTag.class);
    _jspx_th_webwork_005fproperty_005f8.setPageContext(_jspx_page_context);
    _jspx_th_webwork_005fproperty_005f8.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_webwork_005fif_005f7);
    // /template/xhtml/checkbox.jsp(12,56) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_webwork_005fproperty_005f8.setValue("parameters['id']");
    int _jspx_eval_webwork_005fproperty_005f8 = _jspx_th_webwork_005fproperty_005f8.doStartTag();
    if (_jspx_th_webwork_005fproperty_005f8.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.reuse(_jspx_th_webwork_005fproperty_005f8);
      return true;
    }
    _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.reuse(_jspx_th_webwork_005fproperty_005f8);
    return false;
  }

  private boolean _jspx_meth_webwork_005fproperty_005f9(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  webwork:property
    com.opensymphony.webwork.views.jsp.PropertyTag _jspx_th_webwork_005fproperty_005f9 = (com.opensymphony.webwork.views.jsp.PropertyTag) _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fescape_005fnobody.get(com.opensymphony.webwork.views.jsp.PropertyTag.class);
    _jspx_th_webwork_005fproperty_005f9.setPageContext(_jspx_page_context);
    _jspx_th_webwork_005fproperty_005f9.setParent(null);
    // /template/xhtml/controlfooter.jsp(3,12) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_webwork_005fproperty_005f9.setValue("parameters['description']");
    // /template/xhtml/controlfooter.jsp(3,12) name = escape type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_webwork_005fproperty_005f9.setEscape(false);
    int _jspx_eval_webwork_005fproperty_005f9 = _jspx_th_webwork_005fproperty_005f9.doStartTag();
    if (_jspx_th_webwork_005fproperty_005f9.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fescape_005fnobody.reuse(_jspx_th_webwork_005fproperty_005f9);
      return true;
    }
    _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fescape_005fnobody.reuse(_jspx_th_webwork_005fproperty_005f9);
    return false;
  }
}
