package org.apache.jsp.template.xhtml;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class hidden_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody;
  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.release();
    _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<input type=\"hidden\"\n");
      out.write("       name=\"");
      if (_jspx_meth_webwork_005fproperty_005f0(_jspx_page_context))
        return;
      out.write("\"\n");
      out.write("         ");
      if (_jspx_meth_webwork_005fif_005f0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("         ");
      if (_jspx_meth_webwork_005fif_005f1(_jspx_page_context))
        return;
      out.write("\n");
      out.write("        />\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_webwork_005fproperty_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  webwork:property
    com.opensymphony.webwork.views.jsp.PropertyTag _jspx_th_webwork_005fproperty_005f0 = (com.opensymphony.webwork.views.jsp.PropertyTag) _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.get(com.opensymphony.webwork.views.jsp.PropertyTag.class);
    _jspx_th_webwork_005fproperty_005f0.setPageContext(_jspx_page_context);
    _jspx_th_webwork_005fproperty_005f0.setParent(null);
    // /template/xhtml/hidden.jsp(20,13) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_webwork_005fproperty_005f0.setValue("parameters['name']");
    int _jspx_eval_webwork_005fproperty_005f0 = _jspx_th_webwork_005fproperty_005f0.doStartTag();
    if (_jspx_th_webwork_005fproperty_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.reuse(_jspx_th_webwork_005fproperty_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.reuse(_jspx_th_webwork_005fproperty_005f0);
    return false;
  }

  private boolean _jspx_meth_webwork_005fif_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  webwork:if
    com.opensymphony.webwork.views.jsp.IfTag _jspx_th_webwork_005fif_005f0 = (com.opensymphony.webwork.views.jsp.IfTag) _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.get(com.opensymphony.webwork.views.jsp.IfTag.class);
    _jspx_th_webwork_005fif_005f0.setPageContext(_jspx_page_context);
    _jspx_th_webwork_005fif_005f0.setParent(null);
    // /template/xhtml/hidden.jsp(21,9) name = test type = null reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_webwork_005fif_005f0.setTest("parameters['nameValue'] != null");
    int _jspx_eval_webwork_005fif_005f0 = _jspx_th_webwork_005fif_005f0.doStartTag();
    if (_jspx_eval_webwork_005fif_005f0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_webwork_005fif_005f0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_webwork_005fif_005f0.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_webwork_005fif_005f0.doInitBody();
      }
      do {
        out.write("value=\"");
        if (_jspx_meth_webwork_005fproperty_005f1(_jspx_th_webwork_005fif_005f0, _jspx_page_context))
          return true;
        out.write('"');
        int evalDoAfterBody = _jspx_th_webwork_005fif_005f0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_webwork_005fif_005f0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.popBody();
      }
    }
    if (_jspx_th_webwork_005fif_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.reuse(_jspx_th_webwork_005fif_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.reuse(_jspx_th_webwork_005fif_005f0);
    return false;
  }

  private boolean _jspx_meth_webwork_005fproperty_005f1(javax.servlet.jsp.tagext.JspTag _jspx_th_webwork_005fif_005f0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  webwork:property
    com.opensymphony.webwork.views.jsp.PropertyTag _jspx_th_webwork_005fproperty_005f1 = (com.opensymphony.webwork.views.jsp.PropertyTag) _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.get(com.opensymphony.webwork.views.jsp.PropertyTag.class);
    _jspx_th_webwork_005fproperty_005f1.setPageContext(_jspx_page_context);
    _jspx_th_webwork_005fproperty_005f1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_webwork_005fif_005f0);
    // /template/xhtml/hidden.jsp(21,67) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_webwork_005fproperty_005f1.setValue("parameters['nameValue']");
    int _jspx_eval_webwork_005fproperty_005f1 = _jspx_th_webwork_005fproperty_005f1.doStartTag();
    if (_jspx_th_webwork_005fproperty_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.reuse(_jspx_th_webwork_005fproperty_005f1);
      return true;
    }
    _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.reuse(_jspx_th_webwork_005fproperty_005f1);
    return false;
  }

  private boolean _jspx_meth_webwork_005fif_005f1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  webwork:if
    com.opensymphony.webwork.views.jsp.IfTag _jspx_th_webwork_005fif_005f1 = (com.opensymphony.webwork.views.jsp.IfTag) _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.get(com.opensymphony.webwork.views.jsp.IfTag.class);
    _jspx_th_webwork_005fif_005f1.setPageContext(_jspx_page_context);
    _jspx_th_webwork_005fif_005f1.setParent(null);
    // /template/xhtml/hidden.jsp(22,9) name = test type = null reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_webwork_005fif_005f1.setTest("parameters['id'] != null");
    int _jspx_eval_webwork_005fif_005f1 = _jspx_th_webwork_005fif_005f1.doStartTag();
    if (_jspx_eval_webwork_005fif_005f1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_webwork_005fif_005f1 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_webwork_005fif_005f1.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_webwork_005fif_005f1.doInitBody();
      }
      do {
        out.write("id=\"");
        if (_jspx_meth_webwork_005fproperty_005f2(_jspx_th_webwork_005fif_005f1, _jspx_page_context))
          return true;
        out.write('"');
        int evalDoAfterBody = _jspx_th_webwork_005fif_005f1.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_webwork_005fif_005f1 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.popBody();
      }
    }
    if (_jspx_th_webwork_005fif_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.reuse(_jspx_th_webwork_005fif_005f1);
      return true;
    }
    _005fjspx_005ftagPool_005fwebwork_005fif_0026_005ftest.reuse(_jspx_th_webwork_005fif_005f1);
    return false;
  }

  private boolean _jspx_meth_webwork_005fproperty_005f2(javax.servlet.jsp.tagext.JspTag _jspx_th_webwork_005fif_005f1, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  webwork:property
    com.opensymphony.webwork.views.jsp.PropertyTag _jspx_th_webwork_005fproperty_005f2 = (com.opensymphony.webwork.views.jsp.PropertyTag) _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.get(com.opensymphony.webwork.views.jsp.PropertyTag.class);
    _jspx_th_webwork_005fproperty_005f2.setPageContext(_jspx_page_context);
    _jspx_th_webwork_005fproperty_005f2.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_webwork_005fif_005f1);
    // /template/xhtml/hidden.jsp(22,57) name = value type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_webwork_005fproperty_005f2.setValue("parameters['id']");
    int _jspx_eval_webwork_005fproperty_005f2 = _jspx_th_webwork_005fproperty_005f2.doStartTag();
    if (_jspx_th_webwork_005fproperty_005f2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.reuse(_jspx_th_webwork_005fproperty_005f2);
      return true;
    }
    _005fjspx_005ftagPool_005fwebwork_005fproperty_0026_005fvalue_005fnobody.reuse(_jspx_th_webwork_005fproperty_005f2);
    return false;
  }
}
