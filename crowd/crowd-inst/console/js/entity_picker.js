
var CROWD_PICKER = new function() {

        // Post the search form according to URL provided
    function postFormAndBindElements(URL, actionButtonName)
    {
        AJS.$.post(URL,
          { searchString: searchStringVal,
            activeFlag: activeVal,
            resultsPerPage: resultsPerPageVal},
                function(data)
                {
                    AJS.$("#results-table").html(data);
                    bindElements(actionButtonName);
                });
    }


    function bindElements(actionButtonName)
    {
        // Clicking on an individual checkbox
        AJS.$("input:checkbox").click(function()
        {
            var isChecked = AJS.$(this).attr("checked");
            if (isChecked)
            {
                AJS.$(this).parent().parent().addClass("relations-row-highlight");

                AJS.$("button:contains('" + actionButtonName + "')").attr("disabled", false);
            }
            else
            {
                AJS.$(this).parent().parent().removeClass("relations-row-highlight");
                AJS.$("#selectAllRelations").attr("checked", isChecked); // uncheck 'check all' since unchecking a checkbox

                // also means that the update button may now need to disappear
                var atleastOneChecked = false;
                AJS.$("input:checkbox[name=selectedEntityNames]").each(function()
                {
                    var itemChecked = AJS.$(this).attr("checked");
                    if (itemChecked)
                    {
                        atleastOneChecked = true;
                    }
                });

                if (atleastOneChecked)
                {
                    AJS.$("button:contains('" + actionButtonName + "')").attr("disabled", false);
                }
                else
                {
                    AJS.$("button:contains('" + actionButtonName + "')").attr("disabled", true);
                }
            }
        });


        // Clicking on the 'check all' checkbox
        AJS.$("#selectAllRelations").click(function()
        {
            var isCheckAll = AJS.$(this).attr("checked");
            AJS.$("input:checkbox[name=selectedEntityNames]").each(function()
            {
                AJS.$(this).attr("checked", isCheckAll);
                if (isCheckAll)
                {
                    // 1st parent is 'td', 2nd parent is 'tr'
                    AJS.$(this).parent().parent().addClass("relations-row-highlight");
                }
                else
                {
                    AJS.$(this).parent().parent().removeClass("relations-row-highlight");
                }
            });

            if (isCheckAll)
            {
                AJS.$("button:contains('" + actionButtonName + "')").attr("disabled", false);
            }
            else
            {
                AJS.$("button:contains('" + actionButtonName + "')").attr("disabled", true);
            }
        });

    }

    return {
        attachPicker : function(buttonId, title, searchURL, actionName, actionURL, viewPickerURL, loginURL, initialMessage, finalURL) {
            AJS.$("#" + buttonId).click(function()
            {

                AJS.$.get(loginURL,
                    function(data)
                    {
                        if (data == "loggedin")
                        {
                            // Start creating the dialog
                            var popup = new AJS.Dialog(800, 600);
                            popup.addHeader(title);
                            popup.addPanel(title, title);


                            AJS.$.get(viewPickerURL,
                                {
                                    initialMessage: initialMessage
                                },
                                function(data)
                                {
                                    popup.getCurrentPanel().html(data);

                                    attachSearchAndAction(searchURL, actionName);
                                    attachActionButton(actionURL, actionName);
                                });

                            popup.addButton(actionName);


                            AJS.$("button:contains('"+actionName+"')").attr("disabled", true);

                            popup.addButton("Cancel",
                                    function (dialog)
                                    {
                                        dialog.remove();
                                    });

                            popup.show();
                        }
                        else
                        {
                            window.location = finalURL;
                        }

                    });
        
            });
        }
    };

    // When the search form is submitted pass through form parameters to specified searchURL action
    function attachSearchAndAction(searchURL, actionButtonName)
    {
        AJS.$("#browseentities").submit(function()
        {
            searchStringVal = AJS.$("#searchString").val();
            activeVal = AJS.$("#activeFlag :selected").val();
            resultsPerPageVal = parseInt(AJS.$("#resultsPerPage :selected").val());

            postFormAndBindElements(searchURL, actionButtonName);

            return false;
        });
    }

    // Create the list of selected entities and goto the action URL
    function attachActionButton(actionURL, actionButtonName)
    {
        AJS.$("button:contains('" + actionButtonName + "')").click(function() {
            AJS.$("#selectentities").attr("action", actionURL);
            AJS.$("#selectentities").submit();
        });
    }
}();

