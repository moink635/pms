<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/webwork" prefix="ww" %>
<html>
<head>
    <title>
        <ww:text name="menu.viewapplication.label"/>
    </title>
    <meta name="section" content="applications"/>
    <meta name="pagename" content="view"/>
    <meta name="help.url" content="<ww:text name="help.application.view.permissions"/>"/>

    <script>

        function viewPermissionMappings(directoryId)
        {
            var form = document.permissionForm;
            form.directoryId.value = directoryId;
            form.action = '<ww:url namespace="/console/secure/application" action="viewpermissions" method="doDefault" includeParams="none" />';
            form.submit();
        }

    </script>
</head>
<body>

<h2 id="application-name">
    <img class="application-icon" style="padding-bottom:3px;" title="<ww:property value="getImageTitle(application.active, application.type)"/>"
         alt="<ww:property value="getImageTitle(application.active, application.type)"/>" src="<ww:property value="getImageLocation(application.active, application.type)" />"/>
    <ww:property value="application.name"/>
</h2>

<div class="page-content">


<ww:component template="application_tab_headers.jsp">
    <ww:param name="pagekey" value="'application-permissions'"/>
</ww:component>


<div class="tabContent static" id="tab5">

    <div class="crowdForm">

        <form method="post" action="<ww:url namespace="/console/secure/application" action="updatePermissions" method="doUpdate" includeParams="none"/>" name="permissionForm">

            <ww:hidden name="%{xsrfTokenName}" value="%{xsrfToken}"/>

            <input type="hidden" name="ID" value="<ww:property value="ID" />"/>
            <input type="hidden" name="tab" value="4"/>
            <input type="hidden" name="directoryId" value="<ww:property value="directoryId" />"/>

            <div class="formBodyNoTop">

                <ww:component template="form_tab_messages.jsp">
                    <ww:param name="tabID" value="4"/>
                </ww:component>

                <p>
                    <ww:property value="getText('application.permission.text')"/>
                </p>

                <fieldset id="directory-list" class="directory-list">
                    <legend>Directories</legend>
                    <select name="directory-select" id="directory-select" class="directory-select" size="17"
                            onchange="viewPermissionMappings(this.options[this.selectedIndex].value)">
                        <ww:iterator value="application.directoryMappings">
                            <option
                                    <ww:if test="directoryId==directory.id">selected</ww:if>
                                    value="<ww:property value="directory.id"/>"><ww:property value="directory.name"/></option>
                        </ww:iterator>
                    </select>
                </fieldset>

                <fieldset id="permission-list" class="permission-list">
                    <legend>Permissions</legend>

                    <ww:if test="directoryId == null">
                        <span style="font-size: 1.1em;">You need to select a directory.</span>
                    </ww:if>
                    <ww:else>
                        <ol>
                            <li>
                                <ww:set name="disabledglobally" value="permissionEnabledGlobally(@com.atlassian.crowd.embedded.api.OperationType@CREATE_GROUP) != true"/>
                                <input name="<ww:property value="@com.atlassian.crowd.embedded.api.OperationType@CREATE_GROUP.name"/>"
                                       id="<ww:property value="@com.atlassian.crowd.embedded.api.OperationType@CREATE_GROUP.name"/>"
                                       value="true"
                                       type="checkbox"
                                        <ww:if test="hasPermission(@com.atlassian.crowd.embedded.api.OperationType@CREATE_GROUP)"> checked="checked"</ww:if>
                                        <ww:if test="disabledglobally"> disabled="disabled"</ww:if>
                                        />
                                <label
                                        <ww:if test="disabledglobally">class="disabled"</ww:if> for="<ww:property value="@com.atlassian.crowd.embedded.api.OperationType@CREATE_GROUP.name"/>">
                                    <ww:text name="permission.addgroup.label"/>
                                    <ww:if test="disabledglobally">(<ww:text name="application.permission.disabledglobally"/>)</ww:if>
                                </label>

                                <div class="permission-description"><ww:text name="permission.addgroup.description"/></div>
                            </li>
                            <li>
                                <ww:set name="disabledglobally" value="permissionEnabledGlobally(@com.atlassian.crowd.embedded.api.OperationType@CREATE_USER) != true"/>
                                <input name="<ww:property value="@com.atlassian.crowd.embedded.api.OperationType@CREATE_USER.name"/>"
                                       id="<ww:property value="@com.atlassian.crowd.embedded.api.OperationType@CREATE_USER.name"/>"
                                       value="true"
                                       type="checkbox"
                                        <ww:if test="hasPermission(@com.atlassian.crowd.embedded.api.OperationType@CREATE_USER)"> checked="checked"</ww:if>
                                        <ww:if test="disabledglobally"> disabled="disabled"</ww:if>
                                        />
                                <label
                                        <ww:if test="disabledglobally">class="disabled"</ww:if> for="<ww:property value="@com.atlassian.crowd.embedded.api.OperationType@CREATE_USER.name"/>">
                                    <ww:text name="permission.addprincipal.label"/>
                                    <ww:if test="disabledglobally">(<ww:text name="application.permission.disabledglobally"/>)</ww:if>
                                </label>

                                <div class="permission-description"><ww:text name="permission.addprincipal.description"/></div>
                            </li>
                            <li>
                                <ww:set name="disabledglobally" value="permissionEnabledGlobally(@com.atlassian.crowd.embedded.api.OperationType@UPDATE_GROUP) != true"/>
                                <input name="<ww:property value="@com.atlassian.crowd.embedded.api.OperationType@UPDATE_GROUP.name"/>"
                                       id="<ww:property value="@com.atlassian.crowd.embedded.api.OperationType@UPDATE_GROUP.name"/>"
                                       value="true"
                                       type="checkbox"
                                        <ww:if test="hasPermission(@com.atlassian.crowd.embedded.api.OperationType@UPDATE_GROUP)"> checked="checked"</ww:if>
                                        <ww:if test="disabledglobally"> disabled="disabled"</ww:if>
                                        />
                                <label
                                        <ww:if test="disabledglobally">class="disabled"</ww:if> for="<ww:property value="@com.atlassian.crowd.embedded.api.OperationType@UPDATE_GROUP.name"/>">
                                    <ww:text name="permission.modifygroup.label"/>
                                    <ww:if test="disabledglobally">(<ww:text name="application.permission.disabledglobally"/>)</ww:if>
                                </label>

                                <div class="permission-description"><ww:text name="permission.modifygroup.description"/></div>
                            </li>
                            <li>
                                <ww:set name="disabledglobally" value="permissionEnabledGlobally(@com.atlassian.crowd.embedded.api.OperationType@UPDATE_USER) != true"/>
                                <input name="<ww:property value="@com.atlassian.crowd.embedded.api.OperationType@UPDATE_USER.name"/>"
                                       id="<ww:property value="@com.atlassian.crowd.embedded.api.OperationType@UPDATE_USER.name"/>"
                                       value="true"
                                       type="checkbox"
                                        <ww:if test="hasPermission(@com.atlassian.crowd.embedded.api.OperationType@UPDATE_USER)"> checked="checked"</ww:if>
                                        <ww:if test="disabledglobally"> disabled="disabled"</ww:if>
                                        />
                                <label
                                        <ww:if test="disabledglobally">class="disabled"</ww:if> for="<ww:property value="@com.atlassian.crowd.embedded.api.OperationType@UPDATE_USER.name"/>">
                                    <ww:text name="permission.modifyprincipal.label"/>
                                    <ww:if test="disabledglobally">(<ww:text name="application.permission.disabledglobally"/>)</ww:if>
                                </label>

                                <div class="permission-description"><ww:text name="permission.modifyprincipal.description"/></div>
                            </li>
                            <li>
                                <ww:set name="disabledglobally" value="permissionEnabledGlobally(@com.atlassian.crowd.embedded.api.OperationType@UPDATE_GROUP_ATTRIBUTE) != true"/>
                                <input name="<ww:property value="@com.atlassian.crowd.embedded.api.OperationType@UPDATE_GROUP_ATTRIBUTE.name"/>"
                                       id="<ww:property value="@com.atlassian.crowd.embedded.api.OperationType@UPDATE_GROUP_ATTRIBUTE.name"/>"
                                       value="true"
                                       type="checkbox"
                                        <ww:if test="hasPermission(@com.atlassian.crowd.embedded.api.OperationType@UPDATE_GROUP_ATTRIBUTE)"> checked="checked"</ww:if>
                                        <ww:if test="disabledglobally"> disabled="disabled"</ww:if>
                                        />
                                <label
                                        <ww:if test="disabledglobally">class="disabled"</ww:if> for="<ww:property value="@com.atlassian.crowd.embedded.api.OperationType@UPDATE_GROUP_ATTRIBUTE.name"/>">
                                    <ww:text name="permission.modifygroupattribute.label"/>
                                    <ww:if test="disabledglobally">(<ww:text name="application.permission.disabledglobally"/>)</ww:if>
                                </label>

                                <div class="permission-description"><ww:text name="permission.modifygroupattribute.description"/></div>
                            </li>
                            <li>
                                <ww:set name="disabledglobally" value="permissionEnabledGlobally(@com.atlassian.crowd.embedded.api.OperationType@UPDATE_USER_ATTRIBUTE) != true"/>
                                <input name="<ww:property value="@com.atlassian.crowd.embedded.api.OperationType@UPDATE_USER_ATTRIBUTE.name"/>"
                                       id="<ww:property value="@com.atlassian.crowd.embedded.api.OperationType@UPDATE_USER_ATTRIBUTE.name"/>"
                                       value="true"
                                       type="checkbox"
                                        <ww:if test="hasPermission(@com.atlassian.crowd.embedded.api.OperationType@UPDATE_USER_ATTRIBUTE)"> checked="checked"</ww:if>
                                        <ww:if test="disabledglobally"> disabled="disabled"</ww:if>
                                        />
                                <label
                                        <ww:if test="disabledglobally">class="disabled"</ww:if> for="<ww:property value="@com.atlassian.crowd.embedded.api.OperationType@UPDATE_USER_ATTRIBUTE.name"/>">
                                    <ww:text name="permission.modifyprincipalattribute.label"/>
                                    <ww:if test="disabledglobally">(<ww:text name="application.permission.disabledglobally"/>)</ww:if>
                                </label>

                                <div class="permission-description"><ww:text name="permission.modifyprincipalattribute.description"/></div>
                            </li>
                            <li>
                                <ww:set name="disabledglobally" value="permissionEnabledGlobally(@com.atlassian.crowd.embedded.api.OperationType@DELETE_GROUP) != true"/>
                                <input name="<ww:property value="@com.atlassian.crowd.embedded.api.OperationType@DELETE_GROUP.name"/>"
                                       id="<ww:property value="@com.atlassian.crowd.embedded.api.OperationType@DELETE_GROUP.name"/>"
                                       value="true"
                                       type="checkbox"
                                        <ww:if test="hasPermission(@com.atlassian.crowd.embedded.api.OperationType@DELETE_GROUP)"> checked="checked"</ww:if>
                                        <ww:if test="disabledglobally"> disabled="disabled"</ww:if>
                                        />
                                <label
                                        <ww:if test="disabledglobally">class="disabled"</ww:if> for="<ww:property value="@com.atlassian.crowd.embedded.api.OperationType@DELETE_GROUP.name"/>">
                                    <ww:text name="permission.removegroup.label"/>
                                    <ww:if test="disabledglobally">(<ww:text name="application.permission.disabledglobally"/>)</ww:if>
                                </label>

                                <div class="permission-description"><ww:text name="permission.removegroup.description"/></div>
                            </li>
                            <li>
                                <ww:set name="disabledglobally" value="permissionEnabledGlobally(@com.atlassian.crowd.embedded.api.OperationType@DELETE_USER) != true"/>
                                <input name="<ww:property value="@com.atlassian.crowd.embedded.api.OperationType@DELETE_USER.name"/>"
                                       id="<ww:property value="@com.atlassian.crowd.embedded.api.OperationType@DELETE_USER.name"/>"
                                       value="true"
                                       type="checkbox"
                                        <ww:if test="hasPermission(@com.atlassian.crowd.embedded.api.OperationType@DELETE_USER)"> checked="checked"</ww:if>
                                        <ww:if test="disabledglobally"> disabled="disabled"</ww:if>
                                        />
                                <label
                                        <ww:if test="disabledglobally">class="disabled"</ww:if> for="<ww:property value="@com.atlassian.crowd.embedded.api.OperationType@DELETE_USER.name"/>">
                                    <ww:text name="permission.removeprincipal.label"/>
                                    <ww:if test="disabledglobally">(<ww:text name="application.permission.disabledglobally"/>)</ww:if>
                                </label>

                                <div class="permission-description"><ww:text name="permission.removeprincipal.description"/></div>
                            </li>
                        </ol>
                    </ww:else>
                </fieldset>
            </div>

            <div class="formFooter wizardFooter">
                <div class="buttons">
                    <input id="update-permissions" name="update-permissions" type="submit" class="button" value="<ww:property value="getText('update.label')"/> &raquo;"/>
                    <input type="button" class="button" value="<ww:property value="getText('cancel.label')"/>"
                           onClick="window.location='<ww:url namespace="/console/secure/application" action="viewpermissions" method="default" includeParams="none"><ww:param name="ID" value="ID"/><ww:param name="tab" value="4"/></ww:url>';"/>
                </div>
            </div>
        </form>

    </div>

</div>

</div>
</body>
</html>