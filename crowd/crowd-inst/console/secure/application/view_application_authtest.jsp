<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/webwork" prefix="ww" %>
<html>
<head>
    <title>
        <ww:text name="menu.viewapplication.label"/>
    </title>
    <meta name="section" content="applications"/>
    <meta name="pagename" content="view"/>
    <meta name="help.url" content="<ww:text name="help.application.view.configtest"/>"/>

</head>
<body>


<h2 id="application-name">
    <img class="application-icon" style="padding-bottom:3px;" title="<ww:property value="getImageTitle(application.active, application.type)"/>"
         alt="<ww:property value="getImageTitle(application.active, application.type)"/>" src="<ww:property value="getImageLocation(application.active, application.type)" />"/>
    <ww:property value="application.name"/>
</h2>

<div class="page-content">

    <ww:component template="application_tab_headers.jsp">
        <ww:param name="pagekey" value="'application-authtest'"/>
    </ww:component>

    <div class="tabContent static" id="tab7">

        <div class="crowdForm">

            <form method="post"
                  action="<ww:url namespace="/console/secure/application" action="viewauthtest" method="configTest" includeParams="none"/>" name="app-auth-test-form">

                <div class="formBody">

                    <ww:component template="form_tab_messages.jsp">
                        <ww:param name="tabID" value="6"/>
                    </ww:component>

                    <input type="hidden" name="ID" value="<ww:property value="ID" />"/>
                    <input type="hidden" name="tab" value="6"/>

                    <p style="margin-bottom: 10px;">
                        <ww:text name="configtest.text.1">
                            <ww:param><ww:property value="application.name"/></ww:param>
                        </ww:text>
                    </p>

                    <div class="row">

                        <ww:if test="testAuthentication == true">

                            <ww:if test="validTestAuthentication == true">
                                <p class="success">
                                    <ww:property value="getText('configtest.validauth.text')"/>
                                </p>
                            </ww:if>
                            <ww:else>
                                <p class="error">
                                    <ww:property value="getText('configtest.invalidauth.text')"/>
                                </p>
                            </ww:else>

                        </ww:if>

                    </div>

                    <ww:textfield name="testUsername">
                        <ww:param name="label" value="getText('principal.name.label')"/>
                    </ww:textfield>

                    <ww:password name="testPassword">
                        <ww:param name="label" value="getText('principal.password.label')"/>
                    </ww:password>

                </div>

                <div class="formFooter wizardFooter">

                    <div class="buttons">

                        <input type="submit" class="button" value="<ww:property value="getText('update.label')"/> &raquo;"/>
                        <input type="button" class="button" value="<ww:property value="getText('cancel.label')"/>"
                               onClick="window.location='<ww:url namespace="/console/secure/application" action="viewauthtest" method="default" includeParams="none" ><ww:param name="ID" value="ID"/><ww:param name="tab" value="6"/></ww:url>'"/>

                    </div>
                </div>


            </form>

        </div>

    </div>

</div>
</body>
</html>