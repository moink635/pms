<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/webwork" prefix="ww" %>

<html>
<head>
    <title>
        <ww:property value="getText('dataimport.importatlassian.title')"/>
    </title>
    <meta name="section" content="dataimport"/>
    <meta name="help.url" content="<ww:property value="getText('help.user.import.atlassian')"/>"/>

</head>

<h2>
    <ww:property value="getText('dataimport.importatlassian.title')"/>
</h2>

<div class="page-content">
    <ul class="tabs">

        <li>
                    <span class="tab">
                        1. <ww:property value="getText('dataimport.type.label')"/>
                    </span>

        </li>

        <li>
                    <span class="tab">
                        2. <ww:property value="getText('dataimport.options.label')"/>
                    </span>
        </li>

        <li class="on">
                    <span class="tab">
                        3. <ww:property value="getText('dataimport.select.source.directory.label')"/>
                    </span>
        </li>

        <li>
                    <span class="tab">
                        4. <ww:property value="getText('dataimport.result.label')"/>
                    </span>
        </li>

    </ul>

    <div class="tabContent static">

        <div class="crowdForm">
            <div class="titleSection">
                <ww:property value="getText('dataimport.importatlassian.source.select')"/>
            </div>

            <form name="dataimport" method="post"
                  action="<ww:url namespace="/console/secure/dataimport" action="importatlassian" method="import" includeParams="none"/>">

                <ww:hidden name="%{xsrfTokenName}" value="%{xsrfToken}"/>

                <input type="hidden" name="configuration.application" value="<ww:property value="configuration.application" />"/>
                <input type="hidden" name="configuration.directoryID" value="<ww:property value="configuration.directoryID" />"/>
                <input type="hidden" name="configuration.importPasswords" value="<ww:property value="configuration.importPasswords" />"/>
                <input type="hidden" name="configuration.databaseURL" value="<ww:property value="configuration.databaseURL" />"/>
                <input type="hidden" name="configuration.databaseDriver" value="<ww:property value="configuration.databaseDriver" />"/>
                <input type="hidden" name="configuration.username" value="<ww:property value="configuration.username" />"/>
                <input type="hidden" name="configuration.password" value="<ww:property value="configuration.password" />"/>

                <div class="formBody">
                    <ww:component template="form_messages.jsp"/>

                    <ww:select name="configuration.sourceDirectoryID" list="sourceDirectories" listKey="id" listValue="name">
                        <ww:param name="label" value="getText('dataimport.directory.sourcedirectory.label')"/>
                        <ww:param name="description">
                            <ww:property value="getText('dataimport.importdirectory.source.description')"/>
                        </ww:param>
                        <ww:param name="required" value="true"/>
                    </ww:select>
                </div>

                <div class="formFooter wizardFooter">

                    <div class="buttons">

                        <input type="submit" class="button"
                               value="<ww:property value="getText('continue.label')"/> &raquo;"/>

                        <input type="button" class="button" value="<ww:property value="getText('cancel.label')"/>"
                                   onClick="window.location='<ww:url namespace="/console/secure/dataimport" action="importtype" method="default" includeParams="none" />';"/>

                    </div>
                </div>

            </form>

        </div>

    </div>
</div>
</body>
</html>