<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/webwork" prefix="ww" %>
<html>
<head>
    <title>
        <ww:property value="getText('menu.viewgroup.label')"/>
    </title>
    <meta name="section" content="groups"/>
    <meta name="pagename" content="viewgroupmembers" />
    <meta name="help.url" content="<ww:property value="getText('help.group.view.users')"/>"/>

    <ww:property value="webResourceManager.requireResource('com.atlassian.auiplugin:jquery')"/>
    <ww:property value="webResourceManager.requireResource('com.atlassian.auiplugin:ajs')"/>
    <ww:property value="webResourceManager.requiredResources" escape="false"/>

    <script>

        AJS.$(document).ready(function(){

            CROWD_PICKER.attachPicker("addUsers", "<ww:property value="getText('picker.addusers.label')"/>",
                    "<ww:url namespace="/console/secure/pickers" action="searchPicker" method="getUserNonMembersOfGroup" includeParams="none" escapeAmp="false"><ww:param name="directoryID" value="directoryID"/><ww:param name="entityName" value="groupName"/></ww:url>",
                    "<ww:property value="getText('picker.addselected.users.label')"/>",
                    "<ww:url namespace="/console/secure/group" action="updatemembers" method="addUsers" includeParams="none" escapeAmp="false"><ww:param name="directoryID" value="directoryID"/><ww:param name="entityName" value="groupName"/></ww:url>",
                    "<ww:url namespace="/console/secure/pickers" action="displayPicker" includeParams="none" />",
                    "<ww:url namespace="/console/secure/pickers" action="checkLoginStatus" includeParams="none" />",
                    "<ww:property value="getText('picker.addusers.start.message')"/>",
                    "<ww:url namespace="/console/secure/group" action="viewmembers" includeParams="none" escapeAmp="false"><ww:param name="directoryID" value="directoryID"/><ww:param name="groupName" value="groupName"/></ww:url>");

            CROWD_PICKER.attachPicker("removeUsers", "<ww:property value="getText('picker.removeusers.label')"/>",
                    "<ww:url namespace="/console/secure/pickers" action="searchPicker" method="getUserMembersOfGroup" includeParams="none" escapeAmp="false"><ww:param name="directoryID" value="directoryID"/><ww:param name="entityName" value="groupName"/></ww:url>",
                    "<ww:property value="getText('picker.removeselected.users.label')"/>",
                    "<ww:url namespace="/console/secure/group" action="updatemembers" method="removeUsers" includeParams="none" escapeAmp="false"><ww:param name="directoryID" value="directoryID"/><ww:param name="entityName" value="groupName"/></ww:url>",
                    "<ww:url namespace="/console/secure/pickers" action="displayPicker" includeParams="none" />",
                    "<ww:url namespace="/console/secure/pickers" action="checkLoginStatus" includeParams="none" />",
                    "<ww:property value="getText('picker.removeusers.start.message')"/>",
                    "<ww:url namespace="/console/secure/group" action="viewmembers" includeParams="none" escapeAmp="false"><ww:param name="directoryID" value="directoryID"/><ww:param name="groupName" value="groupName"/></ww:url>");


            CROWD_PICKER.attachPicker("addGroups", "<ww:property value="getText('picker.addgroups.label')"/>",
                    "<ww:url namespace="/console/secure/pickers" action="searchPicker" method="getGroupNonMembersOfGroup" includeParams="none" escapeAmp="false"><ww:param name="directoryID" value="directoryID"/><ww:param name="entityName" value="groupName"/></ww:url>",
                    "<ww:property value="getText('picker.addselected.groups.label')"/>",
                    "<ww:url namespace="/console/secure/group" action="updatemembers" method="addGroups" includeParams="none" escapeAmp="false"><ww:param name="directoryID" value="directoryID"/><ww:param name="entityName" value="groupName"/></ww:url>",
                    "<ww:url namespace="/console/secure/pickers" action="displayPicker" includeParams="none" />",
                    "<ww:url namespace="/console/secure/pickers" action="checkLoginStatus" includeParams="none" />",
                    "<ww:property value="getText('picker.addgroups.start.message')"/>",
                    "<ww:url namespace="/console/secure/group" action="viewmembers" includeParams="none" escapeAmp="false"><ww:param name="directoryID" value="directoryID"/><ww:param name="groupName" value="groupName"/></ww:url>");

            CROWD_PICKER.attachPicker("removeGroups", "<ww:property value="getText('picker.removegroups.label')"/>",
                    "<ww:url namespace="/console/secure/pickers" action="searchPicker" method="getGroupMembersOfGroup" includeParams="none" escapeAmp="false"><ww:param name="directoryID" value="directoryID"/><ww:param name="entityName" value="groupName"/></ww:url>",
                    "<ww:property value="getText('picker.removeselected.groups.label')"/>",
                    "<ww:url namespace="/console/secure/group" action="updatemembers" method="removeGroups" includeParams="none" escapeAmp="false"><ww:param name="directoryID" value="directoryID"/><ww:param name="entityName" value="groupName"/></ww:url>",
                    "<ww:url namespace="/console/secure/pickers" action="displayPicker" includeParams="none" />",
                    "<ww:url namespace="/console/secure/pickers" action="checkLoginStatus" includeParams="none" />",
                    "<ww:property value="getText('picker.removegroups.start.message')"/>",
                    "<ww:url namespace="/console/secure/group" action="viewmembers" includeParams="none" escapeAmp="false"><ww:param name="directoryID" value="directoryID"/><ww:param name="groupName" value="groupName"/></ww:url>");
        });

    </script>
</head>
<body>
<h2>
    <ww:property value="getText('menu.viewgroup.label')"/>
    &nbsp;&ndash;&nbsp;
    <ww:property value="groupName"/>
</h2>

<div class="page-content">

    <ul class="tabs">

        <li>
            <a href='<ww:url action="view" namespace="/console/secure/group" method="default" includeParams="none">
            <ww:param name="directoryID" value="directoryID"/><ww:param name="name" value="groupName" /> </ww:url>'>
                <ww:property value="getText('menu.details.label')"/>
            </a>
        </li>
        <li class="on">
            <span class="tab"><ww:property value="getText('group.directmembers.label')"/></span>
        </li>
        <ww:if test="supportsNestedGroups">
        <li>
            <a id="view-group-nested-principals"
               href='<ww:url action="viewnestedusers" namespace="/console/secure/group" includeParams="none" ><ww:param name="directoryID" value="directoryID"/><ww:param name="groupName" value="groupName"/></ww:url>'>
                <ww:property value="getText('group.nestedmembers.label')"/>
            </a>
        </li>
        </ww:if>
    </ul>

    <div class="tabContent static">

        <div class="crowdForm">
            <div class="formBody">
                <ww:component template="form_messages.jsp"/>

                <ww:if test="!hasUpdateGroupPermission()">
                    <p class="informationBox">
                        <ww:property value="getText('group.modify.disabled')"/>
                    </p>
                </ww:if>

               <ww:if test="preventingLockout==true">
                   <p class="noteBox">
                       <ww:text name="preventlockout.removeusersfromgroup.label">
                           <ww:param name="0"><ww:property value="remoteUser.name"/></ww:param>
                           <ww:param name="1"><ww:property value="groupName"/></ww:param>
                       </ww:text>
                   </p>
                </ww:if>
                <%--DIRECT SUBGROUPS--%>

                <ww:if test="supportsNestedGroups">

                    <h3><ww:text name="group.groupmembers.label"/></h3>

                    <ww:if test="!subGroups.empty">
                        <table id="view-group-groups" class="members-table">
                            <tr>
                                <th width="30%">
                                    <ww:property value="getText('group.name')"/>
                                </th>
                                <th width="60%">
                                    <ww:property value="getText('group.description.label')"/>
                                </th>
                                <th width="10%">
                                    <ww:property value="getText('group.active.label')"/>
                                </th>
                            </tr>
                            <ww:iterator value="subGroups" status="rowstatus">
                                <ww:if test="#rowstatus.odd == true"><tr class="odd"></ww:if>
                                <ww:else><tr class="even"></ww:else>
                                <td valign="top">
                                    <a href="<ww:url namespace="/console/secure/group" action="view" method="default" includeParams="none"><ww:param name="name" value="name" /><ww:param name="directoryID" value="[1].directoryID" /></ww:url>"
                                       title="<ww:property value="getText('browser.view.label')"/>">
                                        <ww:property value="name"/>
                                    </a>
                                </td>
                                <td valign="top">
                                    <ww:property value="description"/>
                                </td>
                                <td valign="top">
                                    <ww:property value="active"/>
                                </td>                                
                                </tr>
                            </ww:iterator>
                        </table>
                    </ww:if>
                    <ww:else>
                        <p>
                            <ww:text name="viewprincipals.group.nogroups.assigned">
                                <ww:param><ww:property value="groupName"/></ww:param>
                            </ww:text>
                        </p>
                    </ww:else>                    

                    <ww:if test="hasUpdateGroupPermission()">
                        <div class="formFooter wizardFooter" style="padding:1em 0 1em 24em">
                            <div class="buttons">
                                <input id="addGroups" name="addGroups" type="button" class="button" style="width:120px"
                                       value="<ww:property value="getText('picker.addgroups.label')"/>"/>
                                <ww:if test="!subGroups.empty">
                                    <input id="removeGroups" name="removeGroups" type="button" class="button" style="width:120px"
                                           value="<ww:property value="getText('picker.removegroups.label')"/>"/>
                                </ww:if>
                            </div>
                        </div>
                    </ww:if>
                </ww:if>

                
                <%--DIRECT USER MEMBERS --%>

                <h3><ww:text name="group.usermembers.label"/></h3>
                <ww:if test="!principals.empty">
                    <table id="view-group-users" class="members-table">
                        <tr>
                            <th width="30%">
                                <ww:text name="principal.name.label"/>
                            </th>
                            <th width="60%">
                                <ww:text name="principal.email.label"/>
                            </th>
                            <th width="10%">
                                <ww:text name="principal.active.label"/>
                            </th>
                        </tr>

                        <ww:iterator value="principals" status="rowstatus">
                            <ww:if test="#rowstatus.odd == true"><tr class="odd"></ww:if>
                            <ww:else><tr class="even"></ww:else>
                            <td valign="top">
                                <a id="view-principal"
                                   href="<ww:url namespace="/console/secure/user" action="view" method="default" includeParams="none"><ww:param name="name" value="name" /><ww:param name="directoryID" value="[1].directoryID" /></ww:url>"
                                   title="<ww:property value="getText('browser.view.label')"/>">
                                    <ww:property value="name"/>
                                </a>
                            </td>
                            <td valign="top">
                                <ww:property value="emailAddress"/>
                            </td>
                            <td valign="top">
                                <ww:property value="active"/>
                            </td>                            
                            </tr>
                        </ww:iterator>
                    </table>
                </ww:if>
                <ww:else>
                    <p>
                        <ww:text name="viewprincipals.group.noprincipals.assigned">
                            <ww:param><ww:property value="groupName"/></ww:param>
                        </ww:text>
                    </p>
                </ww:else>
            </div>

            <ww:if test="hasUpdateGroupPermission()">
                <div class="formFooter wizardFooter" style="padding:1em 0 1em 24em">
                    <div class="buttons">
                        <input id="addUsers" name="addUsers" type="button" class="button" style="width:120px"
                               value="<ww:property value="getText('picker.addusers.label')"/>"/>
                        <ww:if test="!principals.empty">
                            <input id="removeUsers" name="removeUsers" type="button" class="button" style="width:120px"
                                   value="<ww:property value="getText('picker.removeusers.label')"/>"/>
                        </ww:if>
                    </div>
                </div>
            </ww:if>

        </div>
    </div>

    <script src="<ww:url value="/console/js/entity_picker.js"/>"></script>
</div>

</body>
</html>
