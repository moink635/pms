<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/webwork" prefix="ww" %>

<html>
<head>

    <title></title>

</head>
<body>


<div id="manageEntity" class="crowdForm">

    <ww:component template="form_messages.jsp"/>

    <form id="browseentities" name="browseentities" method="post">
        <p class="miniform">
            <ww:property value="getText('browser.filter.label')"/>
            :&nbsp;<input id="searchString" type="text" name="searchString" size="60"
                          value="<ww:property value="searchString"/>"/>

            <input id="searchButton" name="searchButton" type="submit" class="button"
                   value="<ww:property value=" getText('browser.filter.label')"/>"/>

        </p>

        <p>
            <ww:property value="getText('principal.active.label')"/>
            :&nbsp;
            <select id="activeFlag" name="activeFlag" style="width: 100px;">
                <option value="">
                    <ww:property value="getText('all.label')"/>
                </option>
                <option value="true"
                        <ww:if test="active == 'true'">selected</ww:if>>
                    <ww:property value="getText('active.label')"/>
                </option>
                <option value="false"
                        <ww:if test="active == 'false'">selected</ww:if>>
                    <ww:property value="getText('inactive.label')"/>
                </option>
            </select>

            &nbsp;&nbsp;

            <ww:property value="getText('browser.maximumresults.label')"/>
            :&nbsp;
            <select id="resultsPerPage" name="resultsPerPage" style="width: 60px;">
                <option value="10"
                        <ww:if test="resultsPerPage == 10">selected</ww:if>>10
                </option>
                <option value="20"
                        <ww:if test="resultsPerPage == 20">selected</ww:if>>20
                </option>
                <option value="50"
                        <ww:if test="resultsPerPage == 50">selected</ww:if>>50
                </option>
                <option value="100"
                        <ww:if test="resultsPerPage == 100">selected</ww:if>>100
                </option>
            </select>
        </p>
    </form>

    <form id="selectentities" name="selectentities" method="post">
        <div id="results-table">
            <p class="picker-msg">
                <ww:property value="initialMessage"/>
            </p>
        </div>
    </form>


</div>

</body>

</html>